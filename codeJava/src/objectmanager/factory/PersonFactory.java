package objectmanager.factory;

import model.user.Person;
import model.user.Sector;
import objectmanager.register.Register;

class PersonFactory {
    private final FunctionFactory functionFactory ;

    private PersonFactory(){
        functionFactory = FunctionFactory.getFunctionFactory() ;
    }

    /** Holder**/
    private static class FactoryHolder {
        /** Instance unique non préinitialisée */
        private final static PersonFactory personFactory = new PersonFactory() ;
    }

    public static PersonFactory getPersonFactory(){
        return FactoryHolder.personFactory ;
    }

    /**
     * create and return a Person Object
     * @param userID id of the user as int (Integer)
     * @param firstName firstname of the person as String
     * @param lastName lastname of the person as String
     * @param pseudo alias of the person as String
     * @param function function of the person as String
     * @param graduationYear graduation year as int (Integer) for pupils
     * @param sector sector as Sector for students
     * @return Person object
     * @label createPerson
     */
    public Person createPerson(int userID, String firstName, String lastName, String pseudo, String function, int graduationYear, Sector sector) {
        Person person = Register.getRegister().searchPerson(userID) ;
        if (person==null) {
            person = new Person(userID, firstName, lastName, pseudo);
            functionFactory.addFunction(person, function, graduationYear, sector);
            Register.getRegister().addPerson(person);
        }
        return person;
    }

    /**
     * {@link FunctionFactory addFunction}
     */
    public void addFunction(Person person,String function,int graduationYear,Sector sector){
        functionFactory.addFunction(person,function,graduationYear, sector);
    }

    /**
     * {@link FunctionFactory addFunctionContact}
     */
    public void addFunctionContact(Person person, int idOrga, String phoneNumber, String mailAdress, String state){
        functionFactory.addFunctionContact(person, idOrga, phoneNumber, mailAdress, state);
    }
}

