package objectmanager.factory;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import model.tool.OrganismState;
import model.user.*;
import objectmanager.register.Register;

/**
 * Class used to add function to already created Person Object
 */
class FunctionFactory {
    static final String nullFunction = "null_function" ;

    private FunctionFactory(){
    }

    /** Holder */
    private static class FactoryHolder {
        /** Instance unique non préinitialisée */
        private final static FunctionFactory functionFactory = new FunctionFactory() ;
    }

    /**
     * return the FunctionFactory Object
     * @return FunctionFactory
     */
    public static FunctionFactory getFunctionFactory(){
        return FactoryHolder.functionFactory ;
    }

    /**
     * create an function (Contact function excluded) and add it to the Person Object
     * @param person Person Object
     * @param function function as String
     * @param graduationYear graduation year as int(Integer) for pupils
     * @param sector sector as Sector enum for students
     * @label addFunction
     */
    public void addFunction(Person person, String function, int graduationYear, Sector sector){
        if (person.getRoles() == null) {
            person.setRoles(new SimpleListProperty<>(FXCollections.observableArrayList()));
        }
        switch(function.toLowerCase()){
            case "photo" :
            case "snum" :
            case "info":
                person.getRoles().add(new Student(graduationYear,sector)) ;
                break ;
            case "imr":
            case "apprentice" :
            case "apprenti" :
                person.getRoles().add(new Apprentice(graduationYear)) ;
                break;
            case "departmentofstudies" :
            case "dde" :
                person.getRoles().add(new DepartmentOfEducation()) ;
                break;
            case "departmentofschool" :
            case "scolarité":
                person.getRoles().add(new StudentOffice()) ;
                break;
            case "professor" :
            case "professeur":
            case "enseignant":
                person.getRoles().add(new Professor()) ;
                break;
            case "externalrelationsresponsible" :
            case "responsable":
                person.getRoles().add(new ExternalRelationsResponsible()) ;
                break;
            case "technicalsupport" :
            case "servicetechnique" :
                person.getRoles().add(new TechnicalOffice()) ;
                break;
            case nullFunction:
                break ;
            default:
                person.getRoles().add(new Other(function)) ;
                break;
        }
    }

    /**
     * create an object Contact and add the Contact function to the Person Object
     * @param person Person Object
     * @param idOrga id of the organization as int (Integer)
     * @param phoneNumber phone number as String
     * @param mailAddress mail address as String
     * @param state state of the contact as string
     * @label addFunctionContact
     */
    public void addFunctionContact(Person person, int idOrga, String phoneNumber, String mailAddress, String state) {
        if (person.getRoles() == null) {
            person.setRoles(new SimpleListProperty<>(FXCollections.observableArrayList()));
        }
        OrganismState orgaState;
        ExternalOrganism externalOrganism = Register.getRegister().searchOrganization(idOrga);
        try {

            if (externalOrganism == null) {
                throw new Exception("Organisme inconnu");
            }
            switch (state.toLowerCase()) {
                case "actif":
                    orgaState = OrganismState.operative;
                    break;
                case "blacklisté":
                case "blacklisted":
                    orgaState = OrganismState.blacklisted;
                    break;
                case "inactif":
                default:
                    orgaState =OrganismState.inoperative ;
            }
            if(!hasAlreadyRoleContactFromThisCompany(person, externalOrganism)){
                person.getRoles().add(new Contact(externalOrganism,phoneNumber,mailAddress,orgaState));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private boolean hasAlreadyRoleContactFromThisCompany(Person person, ExternalOrganism externalOrganism) {
        for(int i=0; i< person.getRoles().size(); i++){
            if (person.getRoles().contains(externalOrganism)) {
                return true;
            }
        }
        return false;
    }
}
