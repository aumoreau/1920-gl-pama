package objectmanager.factory;

import model.tool.Grade;
import objectmanager.register.Register;

/**
 * Class used to create the Grade Object. It's based on a singleton pattern.
 */
class GradeFactory {
    private static final GradeFactory gradeFactory = new GradeFactory() ;

    private GradeFactory(){    }

    /**
     * return the object GradeFactory
     * @return GradeFactory object
     */
    public static GradeFactory getGradeFactory(){
        return gradeFactory ;
    }

    /**
     * return an object Grade
     * @param idGrade of the object as int(Integer)
     * @param idContract rade as int(Integer)
     * @param mark report grade as int(Integer)
     * @param label label of the grade as String
     * @return Grade
     * @label createGrade
     */
    public Grade createGrade(Integer idGrade, Integer idContract, Integer mark,String label) {
        Register register = Register.getRegister();
        Grade grade ;
        if (register.searchGrade(idContract) == null) {
            try {
                switch (label.toLowerCase()){
                    case "rapport":
                        grade = new Grade(idGrade,mark,-1,-1);break;
                    case "these":
                        grade = new Grade(idGrade,-1,mark,-1);break;
                    case "travail":
                        grade = new Grade(idGrade,-1,-1,mark);break;
                    default:
                        return null;
                }
                if(grade!=null){
                    register.addGrade(grade,idContract,idGrade,label);
                    register.searchContract(idContract).setGrade(grade);
                }
                return grade;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            grade = register.searchGrade(idContract);
            switch (label.toLowerCase()){
                case "rapport":
                    grade.setReportMark(mark);
                    register.updateGradeMaps(grade,(Integer)idContract,(Integer)idGrade,label);
                    break;
                case "these":
                    grade.setThesisMark(mark);
                    register.updateGradeMaps(grade,(Integer)idContract,(Integer)idGrade,label);
                    break;
                case "travail":
                    grade.setWorkMark(mark);
                    register.updateGradeMaps(grade,(Integer)idContract,(Integer)idGrade,label);
                    break;
            }
            return grade ;
        }
    }


}
