package objectmanager.factory;

import model.tool.Location;

/**
 * Class used to create a Location Object. It's based on a singleton pattern
 */
class LocationFactory {
    private LocationFactory(){ }

    private static class FactoryHolder{
        private static final LocationFactory locationFactory = new LocationFactory() ;
    }

    /**
     * return the LocationFactory object
     * @return LocationFactory
     */
    public static LocationFactory getLocationFactory(){
        return FactoryHolder.locationFactory ;
    }

    /**
     * return a Location object
     * @param country country name as String
     * @param city city name as String
     * @param address address as String
     * @return Location
     */
    public Location createLocation(String country,String city,String address){
        return new Location(country, city, address) ;
    }
}
