package objectmanager.factory;

import model.admin.*;
import model.tool.ContractState;
import model.tool.ContractType;
import model.tool.Grade;
import model.tool.Period;
import model.user.ExternalOrganism;
import model.user.Person;
import objectmanager.register.Register;

import java.util.Date;

/**
 * Class used to create different contract. It's based on a factory and singleton pattern
 */
class ContractFactory {
    private static final ContractFactory contractFactory = new ContractFactory() ;

    private ContractFactory(){    }

    /**
     * return the object ContractFactory
     * @return ContractFactory
     */
    public static ContractFactory getContractFactory(){
        return contractFactory ;
    }

    /**
     * create and return an contract depending on the parameters
     * @param idContract id of the contract as int (Integer)
     * @param studentUserId id of the student as int (Integer)
     * @param contactUserId  id of the contact as int (Integer)
     * @param idOrga id of the external organization as int (Integer)
     * @param startDate date of start of the contract as Date Object
     * @param endDate date of end of the contract as Date Object
     * @param contractType type of the contract as String
     * @param contractState state of the contract as String
     * @return Contract Object
     * @label createContract
     */
    public Contract createContract(
            int idContract,
            int studentUserId,
            int contactUserId,
            int idOrga,
            Date startDate,
            Date endDate,
            String contractType,
            String contractState) {
        Register register = Register.getRegister();
        if (register.searchContract(idContract)==null) {
            try {
                Person student = register.searchPerson(studentUserId); //get Object Person as contract
                Person contact = register.searchPerson(contactUserId);
                ExternalOrganism organism = Register.getRegister().searchOrganization(idOrga);
                ContractState state = this.getState(contractState);
                ContractType type = this.getType(contractType);
                if (student == null) throw new Exception("Unknown student");
                if (contact == null) throw new Exception("Unknown contact");
                if (organism == null) throw new Exception("Unknown organization");
                Period period = new Period(startDate, endDate, new Grade(-1,-1, -1, -1));
                switch (type) {
                    case internshipPFE:
                        return createInternshipPFE(contact,idContract,period,organism,student,state);
                    case academicSemester:
                        return createAcademicSemester(contact, idContract, period, student, organism, state);
                    case internship:
                        return createOtherInternship(contact, idContract, period, student, organism, state);
                    case apprenticeship:
                        return createApprenticeship(contact, idContract, period, student, organism, state);
                    case NaN:
                    default:
                        throw new Exception("Unknown contract type");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }else {
            return register.searchContract(idContract) ;
        }
    }

    private AcademicSemester createAcademicSemester(Person contact, int contractID, Period period, Person pupil, ExternalOrganism organism, ContractState state) {
        AcademicSemester academicSemester = new AcademicSemester(contact, contractID, period, pupil,organism,state);
        Register.getRegister().addContract(academicSemester);
        return academicSemester;
    }

    private Apprenticeship createApprenticeship(Person contact, int contractID, Period period, Person pupil, ExternalOrganism organism, ContractState state) {
        Apprenticeship apprenticeship = new Apprenticeship(contact, contractID, period, pupil,organism,state);
        Register.getRegister().addContract(apprenticeship);
        return apprenticeship;
    }

    private InternshipPFE createInternshipPFE(Person contact, int contractID, Period period, ExternalOrganism organism, Person pupil, ContractState state) {
        InternshipPFE internshipPfe = new InternshipPFE(contact, contractID, period, pupil,organism,state);
        Register.getRegister().addContract(internshipPfe);
        return internshipPfe;
    }

    private OtherInternship createOtherInternship(Person contact, int contractID, Period period, Person pupil, ExternalOrganism organism, ContractState state) {
        OtherInternship otherInternship = new OtherInternship(contact, contractID, period, pupil,organism,state);
        Register.getRegister().addContract(otherInternship);
        return otherInternship;
    }

    private ContractState getState(String contractState){
        contractState = contractState.toLowerCase();
        switch (contractState) {
            case "demandé":
                return ContractState.requested;
            case "en cours":
                return ContractState.ongoing;
            case  "validé":
                return ContractState.accepted;
            case  "rejeté":
                return ContractState.rejected;
            case  "archivé":
                return ContractState.archived;
            default:
                return ContractState.NaN;
        }
    }

    private ContractType getType(String contractType){
        switch (contractType.toLowerCase()) {
            case "stage":
                return ContractType.internship;
            case "stage long":
                return ContractType.internshipPFE;
            case "apprentissage":
                return ContractType.apprenticeship;
            case "semestre":
            case "semestre académique":
                return ContractType.academicSemester;
            default:
                return ContractType.NaN;
        }
    }

}
