package objectmanager.factory;

import model.admin.Firm;
import model.tool.OrganismState;
import model.study.University;
import objectmanager.register.Register;

/**
 * Class used to create an ExternalOrganism Object
 */
class OrganizationFactory {
    private final LocationFactory locationFactory ;

    private OrganizationFactory(){
        locationFactory = LocationFactory.getLocationFactory() ;
    }

    private static class FactoryHolder{
        private final static OrganizationFactory organizationFactory = new OrganizationFactory() ;
    }

    /**
     * return the OrganizationFactory Object
     * @return OrganizationFactory
     */
    public static OrganizationFactory getOrganizationFactory(){
        return FactoryHolder.organizationFactory ;
    }

    /**
     * create and return an University object
     * @param idOrga id of the organization (university)
     * @param name name of the university as String
     * @param country country location as String
     * @param city city location as String
     * @param address address as String
     * @param organismState OrganismState object
     * @return  University object
     * @label createUniversity
     */
    public University createUniversity(int idOrga,String name,String country,String city,String address, OrganismState organismState){
        University university = (University)Register.getRegister().searchOrganization(idOrga);
        if(university==null) {
            university = new University(idOrga, name, (locationFactory.createLocation(country, city, address)), organismState);
            Register.getRegister().addOrganization(university);
        }
        return university ;
    }

    /**
     * create and return an Firm object
     * @param idOrga id of the organization (firm)
     * @param name name of the university as String
     * @param country country location as String
     * @param city city location as String
     * @param address address as String
     * @param organismState OrganismState object
     * @return Firm object
     * @label createCompany
     */
    public Firm createCompany(int idOrga,String siret, String name, String country, String city, String address, OrganismState organismState){
        Firm firm = (Firm) Register.getRegister().searchOrganization(idOrga);
        if (firm==null){
            firm = new Firm(idOrga,siret,name,locationFactory.createLocation(country, city, address), organismState) ;
            Register.getRegister().addOrganization(firm);
        }
        return firm;
    }

}
