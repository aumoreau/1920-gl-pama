package objectmanager.factory;


import model.admin.*;
import model.tool.Grade;
import model.tool.OrganismState;
import model.study.University;
import model.user.Person;
import model.user.Sector;

import java.util.Date;

/**
 * Class used as a facade to all the main factory object. It's based on facade and singleton pattern.
 */
public class Factory {
    private final PersonFactory personFactory;
    private final ContractFactory contractFactory;
    private final OrganizationFactory organizationFactory;
    private final GradeFactory gradeFactory;

    /**
     * Factory private constructor
     */
    private Factory() {
        personFactory = PersonFactory.getPersonFactory();
        contractFactory = ContractFactory.getContractFactory();
        organizationFactory = OrganizationFactory.getOrganizationFactory();
        gradeFactory = GradeFactory.getGradeFactory();
    }
    //Factory holder
    private static class FactoryHolder {
        private static final Factory factory = new Factory();
    }

    /**
     * return the Factory object
     * @return Factory Object
     */
    public static Factory getFactory() {
        return FactoryHolder.factory;
    }

    /**
     * {@link PersonFactory#createPerson createPerson}
     */
    public Person createPerson(int userID, String firstName, String lastName, String pseudo, String function, int graduationYear, Sector sector) {
        return personFactory.createPerson(userID, firstName, lastName, pseudo, function,graduationYear,sector);
    }

    /**
     * {@link PersonFactory createPerson}
     */
    public Person createPerson(int userID, String firstName, String lastName, String pseudo) {
        return this.createPerson(userID, firstName, lastName, pseudo, FunctionFactory.nullFunction,0,Sector.NaN);
    }

    /**
     * {@link OrganizationFactory createUniversity}
     */
    public University createUniversity(int idOrga,String name, String country, String city, String address, OrganismState organismState){
        return organizationFactory.createUniversity(idOrga,name, country, city, address, organismState) ;
    }

    /**
     * {@link OrganizationFactory createCompany}
     */
    public Firm createCompany(int idOrga,String siret, String name, String country, String city, String address, OrganismState organismState){
        return organizationFactory.createCompany(idOrga,siret, name, country, city, address, organismState) ;
    }

    /**
     * {@link ContractFactory createContract}
     */
    public Contract createContract(int idContract, int studentUserId, int contactUserId, int idOrga, Date startDate, Date endDate,
                                   String contractType, String contractState){
        return contractFactory.createContract(idContract, studentUserId, contactUserId,idOrga,startDate,endDate,contractType,contractState);
    }

    /**
     * {@link FunctionFactory addFunction}
     */
    public void addFunction(Person person,String function,int graduationYear,Sector sector){
        personFactory.addFunction(person, function, graduationYear, sector);
    }

    /**
     * {@link FunctionFactory addFunctionContact}
     */
    public void addFunctionContact(Person person, int idOrga, String phoneNumber, String mailAddress, String state){
        personFactory.addFunctionContact(person, idOrga, phoneNumber, mailAddress, state);
    }
    /**
    *{@link GradeFactory createGrade}
    */
     public Grade createGrade(Integer idGrade, Integer idContract, Integer mark,String label){
        return gradeFactory.createGrade(idGrade, idContract,mark,label);
    }
}
