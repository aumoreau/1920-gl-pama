package objectmanager.tools;

/**
 * Exception class raised when an Object is unknown from the register. It can occur when an Object is not created by a factory
 */
public class UnknownObjectException extends Exception {
}
