package objectmanager.tools;

/**
 * This is an exception class used for when a default password is detected
 */
public class DefaultPasswordsException extends Exception {
    /**
     * Default constructor
     */
    public DefaultPasswordsException(){
        super();
    }

    /**
     * Exception constructor
     * @param message Exception message linked with to the exception.
     */
    public DefaultPasswordsException(String message){
        super(message);
    }
}
