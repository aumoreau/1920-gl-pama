package objectmanager.tools;

import objectmanager.tools.csvwriter.ObjectWriter;
import objectmanager.tools.csvreader.ObjectReader;

import java.io.File;

/**
 * Class used as a facade to our file writer and reader. It also uses an Singleton Pattern.
 */
public class FileManager  {
    /**
     * This ObjectWriter object is used to write the csv files.
     */
    ObjectWriter objectWriter ;
    /**
     * This ObjectWriter object is used to read the csv files.
     */
    ObjectReader objectReader ;

    /**
     * Private constructor to ensure the Singleton Pattern.
     */
    private FileManager() {
        objectWriter = ObjectWriter.getObjectWriter();
        objectReader = ObjectReader.getObjectReader();
    }

    /**
     * ObjectHolder Class for the Singleton Pattern
     */
    private static class ObjectHolder{
        private static final FileManager fileManager = new FileManager();
    }

    /**
     * return the FileManager Object.
     * @return FileManager
     */
    public static FileManager getFileManager(){
        return ObjectHolder.fileManager ;
    }

    /**
     * save the database on different csv files.
     */
    public void save(){
        if(FilePath.getFilePath().getDirectorySeated()) {
            objectWriter.saveDatabaseToCSV();
        }
    }

    /**
     * This method permit to load csv files and create the java beans object.
     */
    public void load() {
        if (FilePath.getFilePath().getDirectorySeated()) {
            objectReader.loadObjectFromCSVFile();
        }
    }

/*    /**
     *
     * @param logString Line that have to be added to the
     */
    /*public void log(String logString){
        if(FilePath.getFilePath().getDirectorySeated()) {
            objectWriter.log(logString);
        }
    }*/

    /**
     * checks if a file exists in the system
     * @param filepath filepath to the file we want to check its existence
     * @return boolean
     */
    public static Boolean thisFileExists(String filepath){
        File file = new File(filepath);
        return file.exists();
    }

}
