package objectmanager.tools.csvreader;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import model.tool.OrganismState;
import model.user.Person;
import model.user.Sector;
import objectmanager.factory.Factory;
import objectmanager.tools.FileManager;
import objectmanager.tools.FilePath;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Class used to read the csv files and create the Java Objects. It's based on singleton pattern.
 */
public class ObjectReader {
    /**
     * Factory instance used to create the Java Objects (beans)
     */
    private final Factory factory ;

    /**
     * Private constructor
     */
    private ObjectReader(){
        factory = Factory.getFactory();
    }

    /**
     * Private Object Holder Class
     */
    private static class ObjectHolder{
        private static final ObjectReader objectReader = new ObjectReader();
    }

    /**
     * It gives the ObjectReader instance
     * @return ObjectReader
     */
    public static ObjectReader getObjectReader(){
        return ObjectReader.ObjectHolder.objectReader ;
    }


    /**
     * Read all csv files related to students and create their Person Object
     */
    public void readStudents(){
        if (FileManager.thisFileExists(FilePath.getFilePath().studentCSVFile)) {
            try {
                CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
                CSVReader reader = new CSVReaderBuilder(new FileReader(FilePath.getFilePath().studentCSVFile)).withCSVParser(parser).build();
                String[] nextLine;
                int i = 0;
                while ((nextLine = reader.readNext()) != null) {
                    if (i != 0) {
                        Sector sector ;
                        switch (nextLine[7]) {
                            case "INFO":
                                sector = Sector.INFO;
                                break;
                            case "SNUM":
                                sector = Sector.SNUM;
                                break;
                            case "PHOTO":
                                sector = Sector.PHOTO;
                                break;
                            default:
                                sector = Sector.NaN;
                        }
                        Person person = factory.createPerson(Integer.parseInt(nextLine[1]),
                                nextLine[3], nextLine[4], nextLine[2]);
                        factory.addFunction(person, nextLine[7], Integer.parseInt(nextLine[6]), sector);
                    }
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            System.out.println(FilePath.getFilePath().studentCSVFile +" doesn't exist.");
        }
    }

    /**
     * Read all csv files related to contacts and create their Person Object
     */
    private void readContact() {
        if (FileManager.thisFileExists(FilePath.getFilePath().contactCSVFile)) {
            try {
                CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
                CSVReader reader = new CSVReaderBuilder(new FileReader(FilePath.getFilePath().contactCSVFile)).withCSVParser(parser).build();
                String[] nextLine;
                int i = 0;
                while ((nextLine = reader.readNext()) != null) {
                    if (i != 0) {
                        Person person = factory.createPerson(Integer.parseInt(nextLine[1]),
                                nextLine[3], nextLine[4], nextLine[2]);
                        factory.addFunctionContact(person, Integer.parseInt(nextLine[6]), nextLine[8], nextLine[7], nextLine[9]);
                    }
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            System.out.println(FilePath.getFilePath().contactCSVFile +" doesn't exist.");
        }
    }

    /**
     * Read all csv files related to staff member and create their Person Object
     */
    private void readStaff() {
        if (FileManager.thisFileExists(FilePath.getFilePath().staffCSVFile)) {
            try {
                CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
                CSVReader reader = new CSVReaderBuilder(new FileReader(FilePath.getFilePath().staffCSVFile)).withCSVParser(parser).build();
                String[] nextLine;
                while ((nextLine = reader.readNext()) != null) {
                    //createPerson(int userID, String firstName, String lastName, String pseudo, String function, int graduationYear, Sector sector)
                    factory.createPerson(Integer.parseInt(nextLine[1]),nextLine[2], nextLine[3],
                            nextLine[4],nextLine[5],0,Sector.NaN);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            System.out.println(FilePath.getFilePath().staffCSVFile +" doesn't exist.");
        }
    }

    /**
     * Read all csv files related to universities and create their University Object
     */
    private void readUniversities(){
        if (FileManager.thisFileExists(FilePath.getFilePath().universityCSVFile)) {
            try {
                CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
                CSVReader reader = new CSVReaderBuilder(new FileReader(FilePath.getFilePath().universityCSVFile)).withCSVParser(parser).build();
                String[] nextLine;
                int i = 0;
                while ((nextLine = reader.readNext()) != null) {
                    if (i != 0) {
                        OrganismState state;
                        switch (nextLine[6].toLowerCase()) {
                            case "actif":
                                state = OrganismState.operative;
                                break;
                            case "inactif":
                                state = OrganismState.inoperative;
                                break;
                            default:
                                state = OrganismState.blacklisted;
                                break;
                        }
                        //createUniversity(int idOrga,String name,String country,String city,String address, OrganismState organismState)
                        factory.createUniversity(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[4], nextLine[3], nextLine[2], state);
                    }
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            System.out.println(FilePath.getFilePath().universityCSVFile +" doesn't exist.");
        }
    }

    /**
     * Read all csv files related to companies and create their Firm Object
     */
    private void readCompanies(){
        if(FileManager.thisFileExists(FilePath.getFilePath().companyCSVFile)) {
            try {
                CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
                CSVReader reader = new CSVReaderBuilder(new FileReader(FilePath.getFilePath().companyCSVFile)).withCSVParser(parser).build();
                String[] nextLine;
                int i = 0;
                while ((nextLine = reader.readNext()) != null) {
                    if (i != 0) {
                        OrganismState state;
                        switch (nextLine[6].toLowerCase()) {
                            case "actif":
                                state = OrganismState.operative;
                                break;
                            case "inactif":
                                state = OrganismState.inoperative;
                                break;
                            default:
                                state = OrganismState.blacklisted;
                                break;
                        }
                        //createCompany(int idOrga,String siret, String name, String country, String city, String address, OrganismState organismState)
                        factory.createCompany(Integer.parseInt(nextLine[0]), nextLine[5], nextLine[1], nextLine[4], nextLine[3], nextLine[2], state);
                    }
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            System.out.println(FilePath.getFilePath().companyCSVFile +" doesn't exist.");
        }
    }

    /**
     * Read all csv files related to contracts and create their Contracts Object
     */
    private void readContracts(){
        if(FileManager.thisFileExists(FilePath.getFilePath().contractsCSVFile)) {
            try {
                CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
                CSVReader reader = new CSVReaderBuilder(new FileReader(FilePath.getFilePath().contractsCSVFile)).withCSVParser(parser).build();
                String[] nextLine;
                int i = 0;
                while ((nextLine = reader.readNext()) != null) {
                    if (i != 0) {
                        SimpleDateFormat df1 = new SimpleDateFormat("dd-MMMM-yy", Locale.FRANCE);
                        Date beginningDate = df1.parse(nextLine[4]);
                        Date endingDate = df1.parse(nextLine[5]);
                        //Contract createContract( int idContract,int studentUserId,int contactUserId,
                        // int idOrga,Date startDate,Date endDate,String contractType,String contractState)
                        factory.createContract(Integer.parseInt(nextLine[0]),Integer.parseInt(nextLine[1]),Integer.parseInt(nextLine[2]),
                                Integer.parseInt(nextLine[3]),beginningDate,endingDate,nextLine[6],nextLine[7]);
                    }
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            System.out.println(FilePath.getFilePath().contractsCSVFile +" doesn't exist.");
        }
    }

    /**
     * Read all csv files related to external organisms data and create their ExternalOrganism Object
     */
    private void readOrganisms() {
        this.readCompanies();
        this.readUniversities();
    }

    /**
     * Read all csv files related to people data and create their Person Object
     */
    private void readUsers(){
        this.readStudents();
        this.readContact();
        this.readStaff() ;
    }

    /**
     * Read all the csv files and create their beans (JAVA Object)
     */
    public void loadObjectFromCSVFile() {
        this.readOrganisms();
        this.readUsers();
        this.readContracts();
    }
}
