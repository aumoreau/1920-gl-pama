package objectmanager.tools.csvwriter;

import com.opencsv.CSVWriter;
import objectmanager.tools.FilePath;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class used to copy the contract into csv files from the database. It's based on singleton Pattern.
 */
class ContractWriter extends Writer {
    /**
     * private
     */
    private  ContractWriter(){
        super.connect();
        //this.init();
    }

    public static void newInit() {
        ContractWriter.getObjectWriter().init();
    }

    private static class ObjectHolder{
        private static final ContractWriter objectWriter = new ContractWriter();
    }

    /**
     * return the ContractWriter object
     * @return ContractWriter
     */
    public static ContractWriter getObjectWriter(){
        return ContractWriter.ObjectHolder.objectWriter ;
    }

    private void init(){
        File file = new File(FilePath.getFilePath().contractsCSVFile);
        //Creating the directory
        if(!file.exists()){
            try {
                if (!file.createNewFile()){
                    throw new Exception(String.format("Impossible de créer le fichier : %s",file.getAbsolutePath()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void writeMarks(){
        CSVWriter writer = null;
        ResultSet resultSet = null ;
        try {
            PreparedStatement pst = connection.prepareStatement(
                    String.format("SELECT * FROM %s",
                            attributes.tableMarks));
            resultSet = pst.executeQuery();
            String fileName = FilePath.getFilePath().evaluationsCSVFile;
            Path myPath = Paths.get(fileName);
            writer = new CSVWriter(Files.newBufferedWriter(myPath,
                    StandardCharsets.UTF_8), ';',
                    CSVWriter.NO_ESCAPE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().evaluationsCSVFile));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.writeAll(resultSet, true);
                    writer.close();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
//ontract createContract(
//            int idContract,
//            int studentUserId,
//            int contactUserId,
//            int idOrga,
//            Date startDate,
//            Date endDate,
//            String contractType,
//            String contractState)
//SELECT
//	Contrat.idContrat_Ctr,
//    Eleve.idUt_Ele,
//	Contact.idUt_Cont,
//	Contrat.idOrga_Ctr,
//	Contrat.dateDebut_Ctr,
//	Contrat.dateFin_Ctr,
//	Contrat.type_Ctr,
//	Contrat.etat_Ctr,
//    Contrat.ects_Ctr
//FROM
//	Contrat,
//	Contact,
//    Eleve
//WHERE
//	Contrat.idContact_Ctr = Contact.idContact_Cont
//	AND Contrat.idEle_Ctr = Eleve.idEle_Ele
    private void writeContracts(){
        CSVWriter writer = null;
        ResultSet resultSet = null ;
        try {
            PreparedStatement pst = connection.prepareStatement("SELECT \n" +
                    "\tContrat.idContrat_Ctr,\n" +
                    "    Eleve.idUt_Ele,\n" +
                    "\tContact.idUt_Cont,\n" +
                    "\tContrat.idOrga_Ctr,\n" +
                    "\tContrat.dateDebut_Ctr,\n" +
                    "\tContrat.dateFin_Ctr,\n" +
                    "\tContrat.type_Ctr,\n" +
                    "\tContrat.etat_Ctr,\n" +
                    "    Contrat.ects_Ctr\t\n" +
                    "FROM\n" +
                    "\tContrat,\n" +
                    "\tContact,\n" +
                    "    Eleve\n" +
                    "WHERE\n" +
                    "\tContrat.idContact_Ctr = Contact.idContact_Cont\n" +
                    "\tAND Contrat.idEle_Ctr = Eleve.idEle_Ele\n");
            resultSet = pst.executeQuery();
            String fileName = FilePath.getFilePath().contractsCSVFile;
            Path myPath = Paths.get(fileName);
            writer = new CSVWriter(Files.newBufferedWriter(myPath,
                    StandardCharsets.UTF_8), ';',
                    CSVWriter.NO_ESCAPE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().contractsCSVFile));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.writeAll(resultSet, true);
                    writer.close();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * write all the files related to the contracts
     */
    public void writeAllFiles(){
        this.writeContracts();
        this.writeMarks();
    }
}
