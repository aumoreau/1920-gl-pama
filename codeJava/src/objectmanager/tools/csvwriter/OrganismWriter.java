package objectmanager.tools.csvwriter;

import com.opencsv.CSVWriter;
import objectmanager.tools.FilePath;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

/**
 * Class used to write csv files related to the external organizations
 */
class OrganismWriter extends Writer {
    private final FilePath filePath ;
    private OrganismWriter(){
        filePath = FilePath.getFilePath();
        super.connect();
        //this.init();
    }

    /**
     * check and create the csv files if missing
     */
    public static void newInit() {
        OrganismWriter.getObjectWriter().init();
    }

    private static class ObjectHolder{
        private static final OrganismWriter objectWriter = new OrganismWriter();
    }

    /**
     * return the OrganismWriter object
     * @return OrganismWriter
     */
    public static OrganismWriter getObjectWriter(){
        return OrganismWriter.ObjectHolder.objectWriter ;
    }

    private void init(){
        File file = null;
        //Creating the directory
        for (int i=0;i<3;i++){
            switch (i) {
                case 0:
                    file = new File(filePath.organismCSVFile);
                    break;
                case 1:
                    file = new File(filePath.universityCSVFile);
                    break;
                case 2:
                    file = new File(filePath.companyCSVFile);
                    break;
            }
            if (!file.exists()) {
                try {
                    if (!file.createNewFile()) {
                        throw new Exception(String.format("Impossible de créer le fichier : %s",file.getAbsolutePath()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //this.readme();
        }
    }


    private void writeOrganisms(){
        CSVWriter writer = null;
        ResultSet resultSet = null ;
        try {
            PreparedStatement pst = connection.prepareStatement(
                    String.format("SELECT * FROM %s",
                            attributes.tableOrgaExt));
            resultSet = pst.executeQuery();
            String fileName = filePath.organismCSVFile;
            Path myPath = Paths.get(fileName);
            writer = new CSVWriter(Files.newBufferedWriter(myPath,
                    StandardCharsets.UTF_8), ';',
                    CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().organismCSVFile));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.writeAll(resultSet, true);
                    writer.close();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeUniversities(){
        CSVWriter writer = null;
        ResultSet resultSet = null ;
        try {
            PreparedStatement pst = connection.prepareStatement(
                    String.format("SELECT * FROM %s where %s=?",
                            attributes.tableOrgaExt,attributes.siretOrga));
            pst.setString(1,"Université");
            resultSet = pst.executeQuery();
            String fileName = filePath.universityCSVFile;
            Path myPath = Paths.get(fileName);
            writer = new CSVWriter(Files.newBufferedWriter(myPath,
                    StandardCharsets.UTF_8), ';',
                    CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().universityCSVFile));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.writeAll(resultSet, true);
                    writer.close();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeCompanies(){
        CSVWriter writer = null;
        ResultSet resultSet = null ;
        try {
            PreparedStatement pst = connection.prepareStatement(
                    String.format("SELECT * FROM %s where %s!=?",
                            attributes.tableOrgaExt,attributes.siretOrga));
            pst.setString(1,"Université");
            resultSet = pst.executeQuery();
            String fileName = filePath.companyCSVFile;
            Path myPath = Paths.get(fileName);
            writer = new CSVWriter(Files.newBufferedWriter(myPath,
                    StandardCharsets.UTF_8), ';',
                    CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().companyCSVFile));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.writeAll(resultSet, true);
                    writer.close();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * write all the csv files
     */
    public void writeAllFiles(){
        this.writeCompanies();
        this.writeUniversities();
        this.writeOrganisms();
    }

/*    private void readme() {
        File readme = new File(filePath.readmeFile);
        try {
            if (!readme.exists()) {
                if (!readme.createNewFile()) {
                    throw new Exception("Impossible de créer ce fichier"+readme.getAbsolutePath()) ;
                }else{
                    OutputStreamWriter writer = new OutputStreamWriter(
                            new FileOutputStream(readme), "UTF-8");
                    BufferedWriter bufWriter = new BufferedWriter(writer);
                    bufWriter.write(filePath.readmeFile);
                    bufWriter.newLine();
                    bufWriter.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
