package objectmanager.tools.csvwriter;

import com.opencsv.CSVWriter;
import objectmanager.tools.FilePath;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

/**
 * Class that writes all csv files related to people data
 */
class PersonWriter extends Writer {

    private PersonWriter(){
        super() ;
        super.connect();
        //this.init();
    }

    /**
     * check and create the files if missing
     */
    public static void newInit() {
        PersonWriter.getObjectWriter().init();
    }

    private static class ObjectHolder{
        private static final PersonWriter objectWriter = new PersonWriter();
    }

    /**
     * return the PersonWriter object
     * @return PersonWriter
     */
    public static PersonWriter getObjectWriter(){
        return ObjectHolder.objectWriter ;
    }

    private void init(){
        File file = null ;
        FilePath filePath = FilePath.getFilePath();
        for (int i=0;i<4;i++) {
            switch (i) {
                case 0:
                    file = new File(filePath.usersCSVFile);
                    break;
                case 1:
                    file = new File(filePath.studentCSVFile);
                    break;
                case 2:
                    file = new File(filePath.contactCSVFile);
                    break;
                case 3:
                    file = new File(filePath.staffCSVFile);
                    break;
            }
            //Creating the directory
            if (!file.exists()) {
                try {
                    if (!file.createNewFile()) {
                        throw new Exception(String.format("Impossible de créer le fichier : %s", file.getAbsolutePath()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeStudents() {
        CSVWriter writer = null;
        ResultSet resultSet = null ;
        try {
            PreparedStatement pst = connection.prepareStatement(
                    String.format("SELECT %s,%s,%s,%s,%s,%s,%s,%s FROM Utilisateur, Eleve where %s=%s",
                            attributes.idEleve, attributes.idUt, attributes.identifiant, attributes.lastname,
                            attributes.firstname, attributes.authorization, attributes.graduationYear, attributes.sector,
                            attributes.idUt, attributes.idUt_Ele));
            resultSet = pst.executeQuery();
            String fileName = FilePath.getFilePath().studentCSVFile;
            Path myPath = Paths.get(fileName);
            writer = new CSVWriter(Files.newBufferedWriter(myPath,
                        StandardCharsets.UTF_8), ';',
                        CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
            System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().studentCSVFile));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.writeAll(resultSet, true);
                    writer.close();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void writeContacts(){
        CSVWriter writer = null;
        ResultSet resultSet = null ;
        try {
            PreparedStatement pst = connection.prepareStatement(
                    String.format("SELECT %s,%s,%s,%s,%s,%s,%s,%s,%s,%s FROM Utilisateur,Contact where %s=%s",
                            attributes.idContact, attributes.idUt, attributes.identifiant, attributes.lastname,
                            attributes.firstname, attributes.authorization, attributes.idOrgaContact, attributes.mail,
                            attributes.phone,attributes.etatContact,attributes.idUt, attributes.idUt_Contact));
            resultSet = pst.executeQuery();
            String fileName = FilePath.getFilePath().contactCSVFile;
            Path myPath = Paths.get(fileName);
            writer = new CSVWriter(Files.newBufferedWriter(myPath,
                    StandardCharsets.UTF_8), ';',
                    CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().contactCSVFile));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.writeAll(resultSet, true);
                    writer.close();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeStaff(){
        try {
            //SELECT idPers_Pers,idUt_Ut,prenom_Ut,nom_Ut,identifiant_Ut,fonction_Pers FROM Utilisateur,Personnel WHERE idUt_Ut=idUt_Pers ;
            PreparedStatement pst = connection.prepareStatement(String.format(
                    "SELECT %s,%s,%s,%s,%s FROM %s,%s where %s=%s",attributes.idPersonnel,
                    attributes.idUt,attributes.firstname,attributes.lastname,attributes.identifiant,
                    attributes.tableUser,attributes.tableStaff,attributes.idUt,attributes.idUt_Personnel));
            ResultSet resultSet = pst.executeQuery();
            String fileName = FilePath.getFilePath().staffCSVFile;
            Path myPath = Paths.get(fileName);
            CSVWriter writer = null;
            try {
                writer = new CSVWriter(Files.newBufferedWriter(myPath,
                        StandardCharsets.UTF_8), ';',
                        CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
                System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().staffCSVFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (writer != null) {
                writer.writeAll(resultSet, true);
                writer.close();
            }
        } catch (SQLException | IOException ex) {
            ex.printStackTrace();
        }
    }

    private void writeAllUsers() {
        try {
            PreparedStatement pst = connection.prepareStatement(String.format("SELECT * FROM %s",attributes.tableUser));
            ResultSet resultSet = pst.executeQuery();
            String fileName = FilePath.getFilePath().usersCSVFile;
            Path myPath = Paths.get(fileName);
            CSVWriter writer = null;
            try {
                writer = new CSVWriter(Files.newBufferedWriter(myPath,
                        StandardCharsets.UTF_8), ';',
                        CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
                System.out.println(String.format("Création et écriture du fichier %s terminées",FilePath.getFilePath().usersCSVFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (writer != null) {
                writer.writeAll(resultSet, true);
                writer.close();
            }
        } catch (SQLException | IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * write all the files needed
     */
    public void writeAllFiles(){
        this.writeAllUsers();
        this.writeStaff();
        this.writeContacts();
        this.writeStudents();
    }
}
