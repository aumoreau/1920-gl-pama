package objectmanager.tools.csvwriter;

import objectmanager.tools.FilePath;

import java.io.*;

/**
 * Class used to writes all csv files. It's based on singleton and facade pattern
 */
public class ObjectWriter {
    private final PersonWriter personWriter ;
    private final OrganismWriter organismWriter ;
    private final ContractWriter contractWriter ;


    private ObjectWriter(){
        //this.init();
        personWriter = PersonWriter.getObjectWriter();
        organismWriter = OrganismWriter.getObjectWriter();
        contractWriter = ContractWriter.getObjectWriter() ;
    }

    private static class ObjectHolder{
        private static final ObjectWriter objectWriter = new ObjectWriter();
    }

    /**
     * return the ObjectWriter object
     * @return ObjectWriter
     */
    public static ObjectWriter getObjectWriter(){
        return ObjectHolder.objectWriter ;
    }

    private void init(){
        File directory ;
        FilePath filePath = FilePath.getFilePath();
        String path = filePath.fileDirectory;
        for(int i=0;i<2;i++){
            switch(i) {
                case 0:
                    path = filePath.fileDirectory;
                    break;
                case 1:
                    path = filePath.fileDirectoryCSV;
                    break;
                //case 2:
                    //path = filePath.logDirectoryCSV;
                    //break;
            }
            directory = new File(path) ;
            if(!directory.exists()){
                if(!directory.mkdir()){
                    System.out.println("Impossible de créer"+directory.getAbsolutePath());
                }else{
                    System.out.println("Création du répertoire "+directory.getAbsolutePath());
                }
            }
        }
        //this.initLogFile();
    }

    /**
     * write all the csv files
     */
    public void saveDatabaseToCSV() {
        //this.init();
        personWriter.writeAllFiles();
        organismWriter.writeAllFiles();
        contractWriter.writeAllFiles();
    }
    /*
    private void initLogFile(){
        File log = new File(FilePath.getFilePath().logFile);
        if(!log.exists()){
            try {
                if (!log.createNewFile()) {
                    throw new Exception("Impossible de créer ce fichier"+log.getAbsolutePath()) ;
                }else{
                    FileWriter writer = new FileWriter(log,true);
                    writer.write("Log;UserID;Pseudo;Date and Time;\n");
                    writer.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void log(String logString) {
        FileWriter log = null;
        try {
            log = new FileWriter(FilePath.getFilePath().logFile,true);
            log.write(logString+"\n");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (log!=null){
                try {
                    log.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/

    /**
     * check and create the directory and files
     */
    public static void newInit(){
        ObjectWriter objectWriter = ObjectWriter.getObjectWriter();
        objectWriter.init(); //create the directories
        /*write the csv files*/
        ContractWriter.newInit();
        PersonWriter.newInit();
        OrganismWriter.newInit();
    }

}
