package objectmanager.tools.csvwriter;

import sessionhandler.DatabaseAttributes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This is an abstract class that all writer class inherit.
 */
public abstract class Writer {
    protected static final String usernameOnline = "1920_INFO2_GroupePAMA";
    protected static final String passwordOnline = "4CfgUSp93uQXuyp7PeTK4Qy8jZjZ49LY";
    protected static final String urlOnline = "jdbc:mysql://barn-e-01:3306/1920_INFO2_GroupePAMA";
    protected static final String username = "javaproject";
    protected static final String password = "javaproject";
    protected static final String url = "jdbc:mysql://localhost:3306/1920_INFO2_GroupePAMA";
    final DatabaseAttributes attributes = DatabaseAttributes.getDatabaseAttributes() ;
    protected Connection connection = null ;

    /**
     * Methods to connect ot the database
     */
    protected void connect() {
        try { //Connection à la base de donnée en ligne
            // connect
            Class.forName("com.mysql.jdbc.Driver");
            DriverManager.setLoginTimeout(2); //se connecter à la base locale après deux secondes sans succès
            connection = DriverManager.getConnection(urlOnline, usernameOnline, passwordOnline);
        } catch (Exception e) {
            try{    // se connecter à la base de donnée locale pour des debogage offline
                // connect
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(url, username, password);
            }catch (SQLException | ClassNotFoundException ex){
                ex.printStackTrace();
            }
        }
    }
}
