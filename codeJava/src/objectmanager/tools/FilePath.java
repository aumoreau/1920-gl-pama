package objectmanager.tools;

import objectmanager.tools.csvwriter.ObjectWriter;

import java.io.*;

/**
 * This class is a singleton object class containing all the path to the different csv files.
 * @author tndoye
 */
public class FilePath {
    /**
     *
     */
    private Boolean directorySet ;
    //private Boolean directoryChanged ;
    //Directory
    //public String defaultDirectory = String.format("%s/1920_INFO2_GroupePAMA",System.getProperty("user.home"));
    public String fileDirectory = String.format("%s/1920_INFO2_GroupePAMA",System.getProperty("user.home")); //Main folder directory
    public String fileDirectoryCSV = String.format("%s/CSV Files",fileDirectory); //Main directory
    // --Commented out by Inspection (02/02/2020 03:00):public String logDirectoryCSV = String.format("%s/Log Files",fileDirectory); //CSV directory
    //CSV files
    public String usersCSVFile = String.format("%s/Users.csv",fileDirectoryCSV) ;
    public String studentCSVFile = String.format("%s/Students.csv",fileDirectoryCSV) ;
    public String contactCSVFile = String.format("%s/Contacts.csv",fileDirectoryCSV) ;
    public String staffCSVFile = String.format("%s/Staff.csv",fileDirectoryCSV) ;
    public String organismCSVFile = String.format("%s/Organisms.csv",fileDirectoryCSV) ;
    public String universityCSVFile = String.format("%s/Universities.csv",fileDirectoryCSV) ;
    public String companyCSVFile = String.format("%s/Companies.csv",fileDirectoryCSV) ;
    public String contractsCSVFile = String.format("%s/Contracts.csv",fileDirectoryCSV) ;
    public String evaluationsCSVFile = String.format("%s/Evaluations.csv",fileDirectoryCSV) ;
    //logfile
    /*public String logFile = String.format("%s/logFile.csv",logDirectoryCSV) ;
    public String readmeFile = String.format("%s/README.txt",fileDirectoryCSV) ;
    public final String readMeContent = "À L'ATTENTION DES UTILISATEURS DE LibreOffice\n" +
            "Le fichier Companies.csv n'utilise pas de virgule en comme sépateur\n" +
            "Vous risquez d'avoir un décalage d'une case sur les valeurs de certaines colonnes car " +
            "les adresses comportent des virgules\n" +
            "Merci de votre compréhension." ;*/

    /**
     * Private constructor to ensure the Singleton pattern.
     */
    private FilePath(){
        directorySet = false ;
    }

    /**
     * Object Holder to ensure the Singleton Pattern.
     */
    private static class ObjectHolder{
        private static final FilePath filePath = new FilePath() ;
    }

    /**
     * ensure the Singleton Pattern and permit to get the FilePath Object from other outer classes.
     * @return FilePath Object
     */
    public static FilePath getFilePath(){
        return ObjectHolder.filePath ;
    }

    /**
     * set the directory in which we want to load or save the csv files.
     * It will automaticly move the previous one if there are two more directory changes during this session
     * @param file the directory in which we want to load or save the csv files
     */
    public void setFileDirectory(File file) {
        //Changes all the different paths to the new one
        fileDirectory = String.format("%s/%s",file.getAbsolutePath(),"1920_INFO2_GroupePAMA");
        fileDirectoryCSV = String.format("%s/CSV Files/", fileDirectory);
        //logDirectoryCSV = String.format("%s/Log Files/", fileDirectory);
        usersCSVFile = String.format("%s/Users.csv", fileDirectoryCSV);
        studentCSVFile = String.format("%s/Students.csv", fileDirectoryCSV);
        contactCSVFile = String.format("%s/Contacts.csv", fileDirectoryCSV);
        staffCSVFile = String.format("%s/Staff.csv", fileDirectoryCSV);
        organismCSVFile = String.format("%s/Organisms.csv", fileDirectoryCSV);
        universityCSVFile = String.format("%s/Universities.csv", fileDirectoryCSV);
        companyCSVFile = String.format("%s/Companies.csv", fileDirectoryCSV);
        contractsCSVFile = String.format("%s/Contracts.csv", fileDirectoryCSV);
        evaluationsCSVFile = String.format("%s/Evaluations.csv", fileDirectoryCSV);
        //logFile = String.format("%s/logFile.csv",logDirectoryCSV) ;
        //readmeFile = String.format("%s/README.txt",fileDirectoryCSV) ;
        directorySet = true;
        ObjectWriter.newInit();
    }


    /**
     * return if a directory was previously set during this session.
     * @return boolean
     */
    public Boolean getDirectorySeated() {
        return directorySet;
    }

}
