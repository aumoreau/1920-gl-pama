package objectmanager.register;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import model.user.ExternalOrganism;
import objectmanager.tools.UnknownObjectException;
import java.util.HashMap;

/**
 * Class used to store all the ExternalOrganism. It's based on a singleton pattern
 */
class OrganizationRegister {
    private final ObservableMap<String,ExternalOrganism> organizationRegister ;
    private final HashMap<Integer,String> mapName ;

    private OrganizationRegister(){
        mapName = new HashMap<>() ;
        organizationRegister = FXCollections.observableHashMap() ;
    }


    /** Holder */
    private static class RegisterHolder {
        /** Instance unique non préinitialisée */
        private final static OrganizationRegister organizationRegister = new OrganizationRegister() ;
    }

    /**
     * return the OrganizationRegister Object
     * @return OrganizationRegister
     */
    public static OrganizationRegister getOrganizationRegister(){
        return OrganizationRegister.RegisterHolder.organizationRegister ;
    }

    /**
     * add an ExternalOrganism object to the ObservableMap
     * @param externalOrganism ExternalOrganization to add
     * @label addExternalOrganism
     */
    public void add(ExternalOrganism externalOrganism){
        organizationRegister.put(externalOrganism.getName().toLowerCase(),externalOrganism) ;
        mapName.put(externalOrganism.getIdOrga(),externalOrganism.getName().toLowerCase());
    }

    /**
     * return an ObservableMap of the ExternalOrganism
     * @return ObservableMap
     * @label getObservableMapOrganization
     */
    public  ObservableMap<String,ExternalOrganism> getObservableMap(){
        return organizationRegister ;
    }

    /**
     * return an ExternalOrganism based on its name or a null object
     * @param nameOrga name of the organization we search
     * @return ExternalOrganism
     * @label searchOrganizationName
     */
    public ExternalOrganism searchOrganization(String nameOrga){
        return organizationRegister.get(nameOrga.toLowerCase()) ;
    }

    /**
     * return an ExternalOrganism based on its id or a null object
     * @param idOrga name of the organization we search
     * @return ExternalOrganism
     * @label searchOrganizationID
     */
    public ExternalOrganism searchOrganization(int idOrga){
        return organizationRegister.get(mapName.get(idOrga)) ;
    }

    /**
     * return true if the name of the external organism has changed, false if not
     * @param organism ExternalOrganism Object
     * @return Boolean
     * @throws UnknownObjectException when the ExternalOrganism is not in the register. It can occur
     * when object are created outside of the factory
     * @label namedChanged
     */
    public Boolean nameChanged(ExternalOrganism organism) throws UnknownObjectException {
        if (organizationRegister.containsKey(mapName.get(organism.getIdOrga()).toLowerCase())){
            if(mapName.get(organism.getIdOrga())!=null && mapName.get(organism.getIdOrga())!=organism.getName().toLowerCase()){
                return true ;
            }else{
                return false;
            }
        }else {
          throw new UnknownObjectException();
        }
    }

    /**
     * update the name reference of the external organism
     * @param organism external organism
     * @label updateNameMap
     */
    public void updateNameMap(ExternalOrganism organism) {
        mapName.replace(organism.getIdOrga(),organism.getName().toLowerCase());
    }

    /*
     * remove all the ExternalOrganism which id is different to the entry parameter
     * @param idOrga id of the ExternalOrganism we want to keep
     * @label keepOnly
     *//*
    public void keepOnly(int idOrga){
        Iterator<ExternalOrganism> it =organizationRegister.values().iterator() ;
        ExternalOrganism organism ;
        while (it.hasNext()){
            organism = it.next() ;
            if (!(organism.getName().equals(mapName.get(idOrga)))){
                organizationRegister.remove(organism.getName().toLowerCase());
            }
        }
    }*/

    void flush() {
        organizationRegister.clear();
    }
}
