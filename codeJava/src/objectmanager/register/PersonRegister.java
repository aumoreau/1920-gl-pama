package objectmanager.register;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import model.user.Contact;
import model.user.Person;
import model.user.Role;

import java.util.ArrayList;

/**
 * Class used to store all Person Object created. It's based on a singleton Pattern.
 */
class PersonRegister {
    private final ObservableMap<Integer,Person> personRegister ;

    private PersonRegister(){
        personRegister = FXCollections.observableHashMap() ;
    }


    /** Holder */
    private static class RegisterHolder {
        /** Instance unique non préinitialisée */
        private final static PersonRegister personRegister = new PersonRegister() ;
    }

    /**
     * return the PersonRegister object
     * @return PersonRegister
     */
    public static PersonRegister getPersonRegister(){
        return PersonRegister.RegisterHolder.personRegister ;
    }

    /**
     * add the Person object to the ObservableMap
     * @param person Person object to add
     * @label addPerson
     */
    public void add(Person person){
        personRegister.put(person.getUserID(),person) ;
    }

    /**
     * return the ObservableMap of Person with their id as key
     * @return ObservableMap
     */
    public ObservableMap<Integer,Person> getObservableMap(){
        return personRegister ;
    }

    /**
     * return Person object or null object
     * @param userID id of the Person to search
     * @return Person object
     * @label searchPerson
     */
    public Person search(int userID){
        return personRegister.get(userID);
    }

    /**
     * return an ArrayLists of Person object with Contact as function
     * @return ArraList<Person>
     * @label getAllContacts
     */
    public ArrayList<Person> getAllContacts() {
        ArrayList<Person> contactList=new ArrayList<>();
        for (Person person : getObservableMap().values()) {
            for (Role role : person.getRoles()) {
                if (role instanceof Contact) {
                    contactList.add(person);
                }
            }
        }
        return contactList;
    }


    void flush() {
        personRegister.clear();
    }


}
