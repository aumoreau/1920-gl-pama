package objectmanager.register;

import javafx.collections.ObservableMap;
import model.admin.Contract;
import model.tool.Grade;
import model.user.ExternalOrganism;
import model.user.Person;
import objectmanager.tools.UnknownObjectException;

import java.util.ArrayList;

/**
 * Class used as a facade to all the different register. It's based on the facade and singleton pattern
 */
public class Register {
    private final ContractRegister contracts ;
    private final OrganizationRegister organizations ;
    private final PersonRegister persons ;
    private final GradeRegister grades ;

    private Register(){
        contracts = ContractRegister.getContractRegister() ;
        organizations = OrganizationRegister.getOrganizationRegister() ;
        persons = PersonRegister.getPersonRegister() ;
        grades = GradeRegister.getGradeRegister();
    }

    private static class RegisterHolder {
        /** Instance unique non préinitialisée */
        private final static Register MAIN_REGISTER_FACADE = new Register() ;
    }

    /**
     * return the Register object
     * @return Register.Object
     */
    public static Register getRegister(){
        return Register.RegisterHolder.MAIN_REGISTER_FACADE;
    }

    /**
     * {@link PersonRegister getAllContacts}
     */
    public ArrayList<Person> getAllContacts() { return persons.getAllContacts(); }

    /*
    public ContractRegister getContracts() {
        return contracts;
    }*/
    /*
    public OrganizationRegister getOrganizations() {
        return organizations;
    }*/
    /*
    public PersonRegister getPersons() {
        return persons;
    }*/

 /*   public GradeRegister getMarks() { return grades; }*/

    /**
     * {@link PersonRegister addPerson}
     */
    public void addPerson(Person person){
        persons.add(person);
    }

    /**
     * {@link ContractRegister addContract}
     */
    public void addContract(Contract contract){
        contracts.add(contract);
    }

    /**
     * {@link OrganizationRegister addOrganization}
     */
    public void addOrganization(ExternalOrganism externalOrganism){
        organizations.add(externalOrganism);
    }

    /**
     * {@link GradeRegister addGrade}
     */
    public void addGrade(Grade grade, Integer idContract,Integer idNote,String label){
        grades.add(grade, idContract, idNote, label);
    }

    /*
     * {@link PersonRegister getObservablePerson}
     *//*
    public ObservableMap<Integer,Person> getObservablePerson(){
        return persons.getObservableMap() ;
    }*/

    /**
     * {@link OrganizationRegister getObservableOrganization}
     */
    public ObservableMap<String,ExternalOrganism> getObservableOrganization(){
        return organizations.getObservableMap();
    }

    /**
     * {@link ContractRegister getObservableContract}
     */
    public ObservableMap<Integer,Contract> getObservableContract(){
        return contracts.getObservableMap() ;
    }

    /*
     * {@link GradeRegister getObservableGrade}
     *//*
    public ObservableMap<Integer,Grade> getObservableGrade() { return grades.getObservableMap(); }*/

    /**
     * {@link PersonRegister searchPerson}
     */
    public Person searchPerson(int userID){
        return persons.search(userID) ;
    }

    /*
     * {@link OrganizationRegister searchOrganizationName}
     *//*
    public ExternalOrganism searchOrganization(String nameOrga){
        return organizations.searchOrganization(nameOrga) ;
    }*/

    /**
     * {@link OrganizationRegister searchOrganizationID}
     */
    public ExternalOrganism searchOrganization(int idOrga){ return  organizations.searchOrganization(idOrga);}

    /**
     * {@link ContractRegister searchContract}
     */
    public Contract searchContract(int idContract){
        return contracts.searchContract(idContract) ;
    }

    /**
     * {@link GradeRegister searchGrade}
     */
    public Grade searchGrade(int idGrade) { return  grades.searchGrade(idGrade); }

    /**
     * {@link GradeRegister namedChanged}
     */
    public boolean namedChanged(ExternalOrganism organism) throws UnknownObjectException {
        return organizations.nameChanged(organism);
    }

    /**
     * {@link GradeRegister updateNameMap}
     */
    public void updateNameMap(ExternalOrganism organism) {
        organizations.updateNameMap(organism) ;
    }

    /**
     * {@link GradeRegister updateGradeMaps}
     */
    public void updateGradeMaps(Grade grade, Integer idContract, Integer idGrade, String label) {
        grades.updateMaps(grade,idContract,idGrade,label);
    }

    /*
     * {@link OrganizationRegister keepOnly}
     *//*
    public void keepOnly(int idOrga) {
        organizations.keepOnly(idOrga);
    }*/

    public void flushAllRegisters(){
        organizations.flush() ;
        persons.flush() ;
        contracts.flush() ;
        grades.flush();
    }

}
