package objectmanager.register;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import model.tool.Grade;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used to store all grade created. It's based on singleton pattern
 */
class GradeRegister{
    /**
     * ObservableMap of the grade with idContracts as key and Grade Object as Object
     */
    protected final ObservableMap<Integer, Grade> gradeRegister ;
    private HashMap<Integer,Integer> idMap ;
    private HashMap<Integer,String> labelMap ;

    private GradeRegister(){
        labelMap = new HashMap<>() ;
        idMap = new HashMap<>() ;
        gradeRegister  = FXCollections.observableHashMap() ;
    }


    /** Holder */
    private static class RegisterHolder {
        /** Instance unique non préinitialisée */
        private final static GradeRegister gradeRegister = new GradeRegister() ;
    }

    /**
     * return the object GradeRegister
     * @return GradeRegister
     */
    public static GradeRegister getGradeRegister(){
        return RegisterHolder.gradeRegister ;
    }

    /*
     * return an ObservableMap of Grade with their id as key
     * @return ObservableMap
     *//*
    public ObservableMap<Integer,Grade> getObservableMap(){ return gradeRegister ; }*/

    /**
     * add a Grade object into the ObservableMap
     * @param grade Grade object to add
     * @param idContract key of the Grade object
     * @label addGrade
     */
    public void add(Grade grade, Integer idContract,Integer idNote,String label){
        labelMap.put(idNote,label);
        idMap.put(idNote,idContract);
        gradeRegister.put(idContract, grade) ;

    }

    /**
     * return the Grade Object or null object
     * @param idContract id or key of the Grade object we search
     * @return  Grade Object
     * @label searchGrade
     */
    public Grade searchGrade(int idContract){
        return gradeRegister.get(idContract) ;
    }

    /**
     *
     * @param grade Grade Object
     * @param idContract id of the contract
     * @param idGrade id of the grade
     * @param label label of the grade
     * @label updateMaps
     */
    void updateMaps(Grade grade, Integer idContract, Integer idGrade, String label) {
        idMap.put(idGrade,idContract);
        labelMap.put(idGrade,label);
        gradeRegister.put(idContract,grade);
    }

    /**
     * clear all the hashMap
     */
    void flush() {
        gradeRegister.clear();
        idMap.clear();
        labelMap.clear();
    }

}
