package objectmanager.register;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import model.admin.Contract;

/**
 * Class used to store all contracts created. It's based on singleton pattern
 */
class ContractRegister{
    /**
     * ObservableMap of the Contracts with id as key and Grade Object as Object
     */
    protected final ObservableMap<Integer,Contract> contractRegister ;

    private ContractRegister(){
        contractRegister  = FXCollections.observableHashMap() ;
    }

    /** Holder */
    private static class RegisterHolder {
        /** Instance unique non préinitialisée */
        private final static ContractRegister contractRegister = new ContractRegister() ;
    }

    /**
     * return the ContractRegister object
     * @return ContractRegister
     */
    public static ContractRegister getContractRegister(){
        return RegisterHolder.contractRegister ;
    }

    /**
     * return an ObservableMap of Contract with their id as key
     * @return ObservableMap
     * @label getObservableMapContract
     */
    public ObservableMap<Integer,Contract> getObservableMap(){ return contractRegister ; }

    /**
     * add a Contract object to the ObservableMap
     * @param contract contract to add
     * @label addContract
     */
    public void add(Contract contract){
        contractRegister.put(contract.getContractID(),contract) ;
    }

    /**
     * return a Contract or a null object
     * @param idContract key of contract in the Map
     * @return Contract Object
     * @label searchContract
     */
    public Contract searchContract(int idContract){
        return contractRegister.get(idContract) ;
    }


    void flush() {
        contractRegister.clear();
    }
}
