package model.tool;

import javafx.beans.property.SimpleObjectProperty;
import java.util.Date;

/**
 * Class that contains the period (of internship) and the evaluation.
 */
public class Period {
    private final SimpleObjectProperty<Date> startDate = new SimpleObjectProperty<>(this, "startDate");
    private final SimpleObjectProperty<Date> endDate = new SimpleObjectProperty<>(this, "endDate");
    private final SimpleObjectProperty<Grade> grade = new SimpleObjectProperty<>(this, "grade");

    /**
     * CLass constructor use to represent the period of a contract
     * @param startDate Object Date beginning of the period
     * @param endDate Object Date ending of the period
     * @param grade Object grade for the different evaluations.
     */
    public Period(Date startDate, Date endDate, Grade grade) {
        this.startDate.set(startDate);
        this.endDate.set(endDate);
        this.grade.set(grade);
    }

    /**
     * return the starting date of the period
     * @return Date
     */
    public Date getStartDate() {
        return startDate.get();
    }

    /*
     * return the starting date of the contract
     * @return SimpleObjectProperty<Date>
     */
   /* public SimpleObjectProperty<Date> startDateProperty() {
        return startDate;
    }*/

    /*
     * set the starting date of the contract
     * @param startDate Date Object
     */
   /* public void setStartDate(Date startDate) {
        this.startDate.set(startDate);
    } */

    /**
     * return the ending date of the contract
     * @return Date
     */
    public Date getEndDate() {
        return endDate.get();
    }

    /*
     * return the ending date of the contract
     * @return SimpleObjectProperty<Date>
     */
   /* public SimpleObjectProperty<Date> endDateProperty() {
        return endDate;
    }*/

    /*
     * set the ending date of the contract
     * @param endDate Date Object
     */
   /* public void setEndDate(Date endDate) {
        this.endDate.set(endDate);
    } */

    /**
     * return the grade of the period
     * @return Grade
     */
    public Grade getGrade() {
        return grade.get();
    }

    /*
     * return the grade of the period
     * @return SImpleObjectProperty<Grade>
     */
  /*  public SimpleObjectProperty<Grade> gradeProperty() {
        return grade;
    } */

    /**
     * set the grade field of the period
     * @param grade Grade Object
     */
    public void setGrade(Grade grade) {
        this.grade.set(grade);
    }
}
