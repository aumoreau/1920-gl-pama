package model.tool;

/**
 * Class used to represent the state of a contract
 */
public enum ContractState {
    requested { //DEMANDÉ
        @Override
        public String toString() {
            return "Demandé";
        }
    },
    ongoing{ //En COURS
        @Override
        public String toString() {
            return "En cours";
        }
    },
    accepted { //VALIDÉ
        @Override
        public String toString() {
            return "Validé";
        }
    },
    rejected { //REFUSÉ
        @Override
        public String toString() {
            return "Refusé";
        }
    },
    archived { //ARCHIVED
        @Override
        public String toString() {
            return "Archivé";
        }
    },
    NaN //État par défaut, inconnu ou erreur
}
