package model.tool;

/**
 * Class used to represent the State of the organism
 */
public enum OrganismState {
    operative { //ACTIF
        @Override
        public String toString() {
            return "Actif";
        }
    },
    inoperative { //INACTIF
        @Override
        public String toString() {
            return "Inactif";
        }
    },
    blacklisted { //LISTE NOIRE, NON RECOMMANDÉ
        @Override
        public String toString() {
            return "Liste noire";
        }
    }
}
