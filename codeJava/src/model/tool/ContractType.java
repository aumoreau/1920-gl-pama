package model.tool;

/**
 * Class used to represent the type of a contract
 */
public enum ContractType {
    internshipPFE { //PFE ou Contrat long
        @Override
        public String toString() {
            return "Projet de Fin d'Étude";
        }
    },
    internship{ //Stage court
        @Override
        public String toString() {
            return "Stage";
        }
    },
     apprenticeship{ //Contrat d'apprentissage
        @Override
        public String toString() {
            return "Apprentissage";
        }
    },
    academicSemester { //REFUSÉ
        @Override
        public String toString() {
            return "Semestre académique";
        }
    },
    NaN //type par défaut, inconnu ou erreur
}
