package model.tool;

/**
 * Class used to represent a grade
 */
public class Grade {
    private Integer idContract;
    private Integer reportMark;
    private Integer thesisMark;
    private Integer workMark;

    /**
     * CLass use to represent a grade
     * @param idContract of the Object (must equal the id of a contract)
     * @param reportMark grade related to a report assessment
     * @param thesisMark grade related to thesis assessment
     * @param workMark grade related to the work assessment
     */
    public Grade(int idContract, int reportMark, int thesisMark, int workMark) {
        this.idContract = idContract;
        this.reportMark = reportMark;
        this.thesisMark = thesisMark;
        this.workMark = workMark;
    }

    /**
     * return id of the grade
     * @return Integer
     */
    public Integer getIdContract() { return idContract; }

    /**
     * return the value of the report mark
     * @return Integer
     */
    public Integer getReportMark() {
        return reportMark;
    }
/*
    public SimpleIntegerProperty reportMarkProperty() {
        return reportMark;
    }
*/

    /**
     * set the value of the report mark
     * @param reportMark report assessment grade
     */
    public void setReportMark(int reportMark) {
        this.reportMark = reportMark;
    }

    /**
     * return the value of the thesis mark
     * @return Integer
     */
    public Integer getThesisMark() {
        return thesisMark;
    }
/*
    public SimpleIntegerProperty thesisMarkProperty() {
        return thesisMark;
    }
*/

    /**
     * set the value of the thesis mark
     * @param thesisMark thesis grade
     */
    public void setThesisMark(int thesisMark) {
        this.thesisMark = thesisMark;
    }

    /**
     * return the value of the work mark
     * @return Integer
     */
    public Integer getWorkMark() {
        return workMark;
    }
/*
    public SimpleIntegerProperty workMarkProperty() {
        return workMark;
    }
*/

    /**
     * set the value of the work mark
     * @param workMark work assessment grade
     */
    public void setWorkMark(int workMark) {
        this.workMark = workMark;
    }

    /**
     * return the string translation of the grade
     * @return String
     */
    @Override
    public String toString() {
        return String.format("Grade{id=%d,reportMark= %d,thesisMark= %d,workMark= %d}", idContract, reportMark, thesisMark, workMark);
    }
}
