package model.tool;

import javafx.beans.property.SimpleStringProperty;

/**
 * Class used to represent the location (country, city, address) of an external organism
 */
public class Location {
    private SimpleStringProperty country = new SimpleStringProperty(this, "country");
    private SimpleStringProperty city = new SimpleStringProperty(this, "city");
    private SimpleStringProperty address = new SimpleStringProperty(this, "address");

    /**
     *
     * @param country country name as String
     * @param city city name as String
     * @param address address as String
     */
    public Location(String country, String city, String address) {
        this.country.set(country);
        this.city.set(city);
        this.address.set(address);
    }

    /**
     * return the country field of the location
     * @return String
     */
    public String getCountry() {
        return country.get();
    }

    /*
     * return the country field of the location
     * @return SimpleStringProperty
     */
   /* public SimpleStringProperty countryProperty() {
        return country;
    } */

    /**
     * set the country field of the location
     * @param country country name as String
     */
    public void setCountry(String country) {
        this.country.set(country);
    }

    /**
     * return the city field of the location
     * @return String
     */
    public String getCity() {
        return city.get();
    }

    /*
     * return the city field of the location
     * @return SimpleStringProperty
     */
   /* public SimpleStringProperty cityProperty() {
        return city;
    }*/

    /**
     * set the city field of the location
     * @param city city name
     */
    public void setCity(String city) {
        this.city.set(city);
    }

    /**
     * return the address field of the location
     * @return String
     */
    public String getAddress() {
        return address.get();
    }

    /*
     * return the address field of the location
     * @return SimpleStringProperty
     */
  /*  public SimpleStringProperty addressProperty() {
        return address;
    }*/

    /*
     * set the address field of location
     * @param address address as String
     */
   /* public void setAddress(String address) {
        this.address.set(address);
    }*/

    /**
     * return the String translation of location
     * @return String
     */
    @Override
    public String toString() {
        return String.format("%s,%s, %s",address.get(),city.get(),country.get());
    }
}
