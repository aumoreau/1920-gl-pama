package model.study;

import model.tool.Location;
import model.user.ExternalOrganism;
import model.tool.OrganismState;

/**
 * Class used to represent a university
 */
public class University extends ExternalOrganism {
    /**
     * CLass use to represent an University type of External organism
     * @param idOrga id of the external organism
     * @param name name of the external organism
     * @param location Location Object
     * @param organismState OrganismState enum
     */
    public University(int idOrga,String name, Location location, OrganismState organismState) {
        this.idOrga.set(idOrga);
        this.setLocation(location);
        this.setName(name);
        this.setState(organismState);
    }

    /**
     * Used only by view Controller
     */
    public University() {}

    /**
     * return string translation of university
     * @return String
     */
    @Override
    public String toString() {
        return "University named "+name.get()+" located in "+location.get()+" and currently "+state.get()+"\n";
    }

    /**
     * return type of external organism
     * @return String
     */
    public String getType()
    {
        return "Université";
    }

    /**
     * return siret for a university
     * @return String
     */
    public String getSiret()
    {
        return "Pas de S.I.R.E.T.";
    }
}
