package model.admin;

import model.tool.ContractState;
import model.tool.Grade;
import model.tool.Period;
import model.user.ExternalOrganism;
import model.user.Person;

/**
 * Class used to represent an academic semester type of contract
 */
public class AcademicSemester extends Contract {
    /**
     * AcademicSemester Constructor
     * @param contact Person Object
     * @param contractID id of the contract as int (Integer)
     * @param period Period Object
     * @param pupil Person Object
     * @param organism ExternalOrganism Object
     * @param state ContractState Enum
     */
    public AcademicSemester(Person contact, int contractID, Period period, Person pupil, ExternalOrganism organism, ContractState state) {
        super.setContact(contact);
        super.setContractID(contractID);
        super.setPeriod(period);
        super.setPupil(pupil);
        super.setState(state);
        super.setOrganism(organism);
        super.setGrade(new Grade(0,-1,-1,-1));
    }

    /**
     * return string describing the academic semester
     * @return String
     */
    @Override
    public String toString() {
        return ("Academic Semester - contract ID"+contractID.get()+"\nPeriods:"+period.get()+"\nStudent:"+
                pupil.get()+"\nTutor:"+contact.get()+"\nStatus:"+state.get()) ;
    }

    /**
     * return type of contract
     * @return String
     */
    @Override
    public String getType() {
        return "Semestre académique";
    }
}
