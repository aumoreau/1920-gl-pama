package model.admin;

import model.tool.ContractState;
import model.tool.Grade;
import model.tool.Period;
import model.user.ExternalOrganism;
import model.user.Person;

/**
 * Class used to represent the regular internship
 */
public class OtherInternship extends Contract {

    /**
     * CLass use to reprensent a regular internship type of contract
     * @param contact Person Object as contact
     * @param contractID id of the Contract as int (Integer)
     * @param period Period Object
     * @param pupil Person Object as student
     * @param organism ExternalOrganism Object
     * @param state ContractState
     */
    public OtherInternship(Person contact, int contractID, Period period, Person pupil, ExternalOrganism organism, ContractState state) {
        super.setContact(contact);
        super.setContractID(contractID);
        super.setPeriod(period);
        super.setPupil(pupil);
        super.setState(state);
        super.setOrganism(organism);
        super.setGrade(new Grade(0,-1,-1,-1));
    }

    /**
     * return string translation of an otherinternship
     * @return String
     */
    @Override
    public String toString() {
        return String.format("Internship - contract ID%s\nPeriods:%s\nStudent:%s\nTutor:%s\nStatus:%s\n", contractID.get(), period.get(),
                pupil.get(),contact.get(),state.get());
        }

    /**
     * return type of contract
     * @return String
     */
    @Override
    public String getType() {
        return "Stage";
    }
}
