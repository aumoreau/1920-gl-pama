package model.admin;

import javafx.beans.property.SimpleStringProperty;
import model.tool.Location;
import model.tool.OrganismState;
import model.user.ExternalOrganism;

/**
 * Class used to represent a company
 */
public class Firm extends ExternalOrganism {
    private final SimpleStringProperty siret = new SimpleStringProperty(this, "siret");

    /**
     * Used only by view Controller
     */
    public Firm (){}

    /**
     * External organism of subtype company
     * @param idOrga id of the external organism as int (Integer)
     * @param siret siret of the external organism as String
     * @param name name of the the external organism
     * @param location Location Object
     * @param organismState OrganismState Enum
     */
    public Firm(int idOrga,String siret, String name, Location location, OrganismState organismState) {
        this.idOrga.set(idOrga);
        this.siret.set(siret);
        this.setLocation(location);
        this.setName(name);
        this.setState(organismState);
    }

    /**
     * return Siret
     * @return String
     */
    @Override
    public String getSiret() {
        return siret.get();
    }

    /*
     * return siret
     * @return SimpleStringProperty
     */
   /* public SimpleStringProperty siretProperty() {
        return siret;
    }*/

    /**
     * set siret
     * @param siret siret of the external organism
     */
    public void setSiret(String siret) {
        this.siret.set(siret);
    }

    /**
     * return string translation of a firm
     * @return String
     */
    @Override
    public String toString() {
        return String.format("Firm named %s(siret:%s) located in %s and currently %s\n", name.get(), siret.get(), location.get(), state.get());
    }

    /**
     * return type of external organism
     * @return String
     */
    public String getType()
    {
        return "Entreprise";
    }
}
