package model.admin;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import model.tool.ContractState;
import model.tool.Grade;
import model.tool.Period;
import model.user.ExternalOrganism;
import model.user.Person;

/**
 * Class that all contract classes inherit
 */
public abstract class Contract {
    protected final SimpleIntegerProperty contractID = new SimpleIntegerProperty(this, "contractID");
    protected final SimpleObjectProperty<Person> pupil = new SimpleObjectProperty<>(this, "pupil");
    protected final SimpleObjectProperty<Person> contact = new SimpleObjectProperty<>(this, "contact");
    protected final SimpleObjectProperty<Period> period = new SimpleObjectProperty<>(this, "period");
    protected final SimpleObjectProperty<Grade> grade = new SimpleObjectProperty<>(this, "grade");
    protected final SimpleObjectProperty<ContractState> state = new SimpleObjectProperty<>(this, "state"); //voir MaBibliothèque
    protected final SimpleObjectProperty<ExternalOrganism> organism = new SimpleObjectProperty<>(this, "organism");

    /**
     * return id of the contract
     * @return int
     */
    public int getContractID() {
        return contractID.get();
    }

    /*
     * return id of the contract
     * @return SimpleIntegerProperty
     */
   /* public SimpleIntegerProperty contractIDProperty() {
        return contractID;
    }*/

    /**
     * set id of the contract
     * @param contractID id of the contract
     */
    public void setContractID(int contractID) {
        this.contractID.set(contractID);
    }

    /**
     * return pupil linked to the contract
     * @return Person
     */
    public Person getPupil() {
        return pupil.get();
    }

    /*
     * return pupil linked to the contract
     * @return SimpleObjectProperty<Person>
     */
   /* public SimpleObjectProperty<Person> pupilProperty() {
        return pupil;
    }*/

    /**
     * set pupil linked to the contract
     * @param pupil Person Object as Student
     */
    public void setPupil(Person pupil) {
        this.pupil.set(pupil);
    }

    /**
     * return contact linked to the contract
     * @return Person
     */
    public Person getContact() {
        return contact.get();
    }

    /*
     * return contact linked to the contract
     * @return SimpleObjectProperty<Person>
     */
/*    public SimpleObjectProperty<Person> contactProperty() {
        return contact;
    } */

    /**
     * set contact linked to the contract
     * @param contact Person Object
     */
    public void setContact(Person contact) {
        this.contact.set(contact);
    }

    /**
     * return period of the contract
     * @return Period Object
     */
    public Period getPeriod() {
        return period.get();
    }

    /*
      return period of the contract
      @return SimpleObjectProperty<Period>
     */
 /*   public SimpleObjectProperty<Period> periodProperty() {
        return period;
    }*/

    /**
     * set period of the contract
     * @param period Period Object
     */
    public void setPeriod(Period period) {
        this.period.set(period);
    }

    /**
     * get grade of the contract
     * @return Grade
     */
    public Grade getGrade() {
        return grade.get();
    }

    /*
     * get grade of the contract
     * @return SimpleObjectProperty<Grade>
     */
/*    public SimpleObjectProperty<Grade> gradeProperty() {
        return grade;
    }*/

    /**
     * set grade of the contract
     * @param grade Grade Object
     */
    public void setGrade(Grade grade) {
        this.grade.set(grade);
    }

    /**
     * return type of the contract
     * @return String
     */
    public abstract String getType();

    /**
     * return state of the contract
     * @return ContractState
     */
    public ContractState getState() {
        return state.get();
    }

    /*
      return state of the contract
      @return SimpleObjectProperty<ContractState>
     */
  /*  public SimpleObjectProperty<ContractState> stateProperty() {
        return state;
    }*/

    /**
     * set state of the contract
     * @param state of the contract
     */
    public void setState(ContractState state) {
        this.state.set(state);
    }

    /**
     * return the organism linked to the contract
     * @return ExternalOrganism
     */
    public ExternalOrganism getOrganism() {
        return organism.get();
    }

    /*
      return the organism linked to the contract
      @return SimpleObjectProperty<ExternalOrganism>
     */
   /* public SimpleObjectProperty<ExternalOrganism> organismProperty() {
        return organism;
    }*/

    /**
     * set the organism linked to the contract
     * @param organism ExternalOrganism Object
     */
    public void setOrganism(ExternalOrganism organism) {
        this.organism.set(organism);
    }


}
