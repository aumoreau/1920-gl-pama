package model.admin;

import model.tool.ContractState;
import model.tool.Grade;
import model.tool.Period;
import model.user.ExternalOrganism;
import model.user.Person;

/**
 * Class used to represent an before-graduation internship type of contract
 */
public class InternshipPFE extends Contract {

    /**
     * IntershipPFE Constructor
     * @param contact Person Object as contact
     * @param contractID id of the contract
     * @param period Period Object
     * @param pupil Person Object as pupil
     * @param organism ExternalOrganism Object
     * @param state ContractState Enum
     */
    public InternshipPFE(Person contact, int contractID, Period period, Person pupil, ExternalOrganism organism, ContractState state) {
        super.setContact(contact);
        super.setContractID(contractID);
        super.setPeriod(period);
        super.setPupil(pupil);
        super.setState(state);
        super.setOrganism(organism);
        super.setGrade(new Grade(0,-1,-1,-1));
    }

    /**
     * return string translation of PFE
     * @return String
     */
    @Override
    public String toString() {
        return String.format("PFE Internship - contract ID%s\nPeriods:%s\nStudent:%s\nTutor:%s\nStatus:%s\n", contractID.get(),period.get(),
                pupil.get(),contact.get(),state.get());
    }

    /**
     * return type of Contract
     * @return String
     */
    @Override
    public String getType() {
        return "Stage PFE";
    }
}
