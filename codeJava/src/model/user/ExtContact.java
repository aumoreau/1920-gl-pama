package model.user;

import model.tool.OrganismState;

public class ExtContact extends Contact {

    public ExtContact(ExternalOrganism externalOrganism, String phoneNumber, String mailAddress, OrganismState organismState) {
        super(externalOrganism, phoneNumber, mailAddress, organismState);
    }
    
}
