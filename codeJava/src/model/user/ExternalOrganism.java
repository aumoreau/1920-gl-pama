package model.user;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import model.tool.Location;
import model.tool.OrganismState;

/**
 * Class used to represent an external organization
 */
public abstract class ExternalOrganism {
    protected final SimpleIntegerProperty idOrga = new SimpleIntegerProperty(this, "idOrga");
    protected SimpleStringProperty name = new SimpleStringProperty(this, "name");
    protected SimpleObjectProperty<Location> location = new SimpleObjectProperty<>(this, "location");
    protected SimpleObjectProperty<OrganismState> state = new SimpleObjectProperty<>(this, "state"); //voir MaBibliothèque

    /**
     * return the name of the external organization
     * @return String
     */
    public String getName() {
        return name.get();
    }

    /**
     * return the name of the external organization
     * @return SimpleStringProperty
     */
    public SimpleStringProperty nameProperty() {
        return name;
    }

    /**
     * set the name field of the organism
     * @param name name as String
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * return the location field of the external organism
     * @return Location Object
     */
    public Location getLocation() {
        return location.get();
    }

    /**
     * return the location field of the external organism
     * @return SimpleObjectProperty<Location>
     */
    public SimpleObjectProperty<Location> locationProperty() {
        return location;
    }

    /**
     * set the location field of the external organism
     * @param location Location Object
     */
    public void setLocation(Location location) {
        this.location.set(location);
    }

    /**
     * return the state of the external organism
     * @return OrganismState
     */
    public OrganismState getState() {
        return state.get();
    }

    /*
     * return the state of the external organism
     * @return SimpleObjectProperty<OrganismState>
     *//*
    public SimpleObjectProperty<OrganismState> stateProperty() {
        return state;
    }*/

    /**
     * set the state field of the external organism
     * @param organismState OrganismState enum
     */
    public void setState(OrganismState organismState) {
        this.state.set(organismState);
    }

    /**
     * return the siret of the external organism
     * @return String
     */
    public String getSiret()
    {
        return "";
    }

    /**
     * return the id of the external organism
     * @return int
     */
    public int getIdOrga() {
        return idOrga.get();
    }

    /*
     * return the id of the external organism
     * @return SimpleIntegerProperty
     *//*
    public SimpleIntegerProperty idOrgaProperty() {
        return idOrga;
    }*/

    /**
     * set the id of the external organism
     * @param idOrga id of the organization as int
     */
    public void setIdOrga(int idOrga) {
        this.idOrga.set(idOrga);
    }

    /**
     * return the type of the external organism
     * @return String
     */
    public String getType()
    {
        return "";
    }
}
