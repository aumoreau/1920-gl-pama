package model.user;

/**
 * Enum of the sector of students
 */
public enum Sector {
    INFO { //Informatique , Computer Science
        @Override
        public String toString() {
            return "Student in Computer Science\n" ;
        }
    },
    PHOTO { //Photonique, Photonics
        @Override
        public String toString() {
            return "Student in Photonics (Science)";
        }
    },
    SNUM { //Science du numérique, digital science
        @Override
        public String toString() {
            return "Student in Digital Science";
        }
    },
    NaN //undefined sector or error
}
