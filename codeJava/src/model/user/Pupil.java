package model.user;

import javafx.beans.property.SimpleIntegerProperty;

/**
 * Class used to all student and apprentice function inherit
 */
public abstract class Pupil implements Role {
    protected final SimpleIntegerProperty graduationYear = new SimpleIntegerProperty(this, "graduationYear");
    //protected int idPupil = 0;

    /*
     * return the graduation year of the pupil
     * @return graduation year as int
     *//*
    public int getGraduationYear() {
        return graduationYear.get();
    }*/

    /*
     * Return graduation property
     * @return SimpleIntegerProperty
     *//*
    public SimpleIntegerProperty graduationYearProperty() {
        return graduationYear;
    }*/

    public void setGraduationYear(int graduationYear) {
        this.graduationYear.set(graduationYear);
    }
}
