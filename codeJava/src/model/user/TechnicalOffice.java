package model.user;

/**
 * Class used to represent the technical office member function
 */
public class TechnicalOffice extends Administration {
    public TechnicalOffice() {
    }

    @Override
    public String toString() {
        return "Technical Support agent" ;
    }
}
