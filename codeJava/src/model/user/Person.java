package model.user;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;

import java.util.ArrayList;

/**
 * Class used to represent a person
 */
public class Person {
    private final SimpleIntegerProperty userID = new SimpleIntegerProperty(this, "userID");
    private final SimpleStringProperty firstName = new SimpleStringProperty(this, "firstName");
    private final SimpleStringProperty lastName = new SimpleStringProperty(this, "lastName");
    private final SimpleStringProperty pseudo = new SimpleStringProperty(this, "pseudo");
    private final SimpleListProperty<Role> roles = new SimpleListProperty<>(this, "roles");
    private int authorization = 1  ;

    /**
     * Person constructor
     * @param userID id of the user/person as int (Integer)
     * @param firstName firstname of the person as String
     * @param lastName lastname of the person as String
     * @param pseudo alias of the person as String
     * @param roles function(s) of the user
     */
    public Person(int userID, String firstName, String lastName, String pseudo, ArrayList<Role> roles) {
        this.userID.set(userID);
        this.firstName.set(firstName);
        this.lastName.set(lastName);
        this.pseudo.set(pseudo);
        this.roles.setAll(roles);
    }

    /**
     * Person constructor
     * @param userID id of the user/person as int (Integer)
     * @param firstName firstname of the person as String
     * @param lastName lastname of the person as String
     * @param pseudo alias of the person as String
     */
    public Person(int userID, String firstName, String lastName, String pseudo) {
        this.userID.set(userID);
        this.firstName.set(firstName);
        this.lastName.set(lastName);
        this.pseudo.set(pseudo);
    }

    /**
     * return the user id of the person
     * @return int
     */
    public int getUserID() {
        return userID.get();
    }

    /*
     * set the user id of the person
     * @param userID
     *//*
    public void setUserID(int userID) {
        this.userID.set(userID);
    }*/

    /**
     * return the firstname of the person
     * @return String
     */
    public String getFirstName() {
        return firstName.get();
    }

    /**
     * set the firstname field of the person
     * @param firstName firstname as String
     */
    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    /**
     * return the lastname of the person
     * @return String
     */
    public String getLastName() {
        return lastName.get();
    }

    /**
     * set the lastname field of the person
     * @param lastName lastname as String
     */
    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    /*
     * return the pseudo of the person
     * @return String
     *//*
    public String getPseudo() {
        return pseudo.get();
    }*/

    /*
     * set the pseudo of the person
     * @param pseudo alias as String
     *//*
    public void setPseudo(String pseudo) {
        this.pseudo.set(pseudo);
    }*/

    /*
     * return the user id of the person
     * @return SimpleIntergerProperty
     /*
    public SimpleIntegerProperty userIDProperty() {
        return userID;
    }*/

    /**
     * return the firstname of the person
     * @return SimpleStringProperty
     */
    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    /**
     * return the lastname of the person
     * @return SimpleStringProperty
     */
    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    /*
     * return the pseudo of the person
     * @return SimpleStringProperty
     *//*
    public SimpleStringProperty pseudoProperty() {
        return pseudo;
    }*/

    /*
     * return the list of role of the person
     * @return SimpleListProperty<Role>
     *//*
    public SimpleListProperty<Role> rolesProperty() {
        return roles;
    }*/

    /**
     * set the list of role field of the person
     * @param roles function(s) of the user
     */
    public void setRoles(ObservableList<Role> roles) {
        this.roles.set(roles);
    }

    /**
     * return the list of role of the perons
     * @return ObservableList<Role>
     */
    public ObservableList<Role> getRoles() {
        return roles.get();
    }

    /**
     * return the authorization level of the person
     * @return int
     */
    public int getAuthorization() {
        return authorization;
    }

    /**
     * set the authorization value of the person
     * @param authorization authorization level of the user
     */
    public void setAuthorization(int authorization) {
        this.authorization = authorization;
    }

    public Person() {
    }

    /*
     * set the role list field of the person
     * @param roles function(s) of the user
     *//*
    public void setRoles(ArrayList<Role> roles) {
        this.roles.setAll(roles);
    }*/

    /**
     * return the string translation of the person
     * @return  String
     */
    @Override
    public String toString(){
        return String.format("UserID:%d\nFirstName:%s\nLastName:%s\nPseudo:%s\nFunctions: %s\n",userID.get(),
                firstName.get(),lastName.get(),pseudo.get(),roles.get());
    }

    /**
     * return the instance of role contact of the person if existence
     * @return Contact
     */
    public Contact getContact() {
        for (Role role : this.roles.getValue()) {
            if(role instanceof Contact){
                return (Contact) role;
            }
        }
        return null ;
    }
}
