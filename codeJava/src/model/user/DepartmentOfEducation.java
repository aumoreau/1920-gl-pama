package model.user;

/**
 * Class used to represent the department of education member function
 */
public class DepartmentOfEducation extends Administration {
    public DepartmentOfEducation() {
    }

    /**
     * return the string translation of the departement of eduction
     * @return String
     */
    @Override
    public String toString(){
        return "Departement of Studies member" ;
    }
}
