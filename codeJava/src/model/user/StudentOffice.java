package model.user;


/**
 * Class used to represent the student office member function
 */
public class StudentOffice extends Administration {
    public StudentOffice() {
    }

    @Override
    public String toString(){
        return "Departement of School member" ;
    }
}
