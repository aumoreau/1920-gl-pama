package model.user;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import model.tool.OrganismState;

/**
 * Class used to represent the contact function
 */
public class Contact implements Role {
    protected SimpleObjectProperty<ExternalOrganism> organization = new SimpleObjectProperty<>(this, "organization");
    protected SimpleStringProperty phoneNumber = new SimpleStringProperty(this, "phoneNumber");
    protected SimpleStringProperty mailAddress = new SimpleStringProperty(this, "mailAddress");
    protected SimpleObjectProperty<OrganismState> state = new SimpleObjectProperty<>(this, "state");

    /**
     * Used only by view Controller
     */
    public Contact(){}

    /**
     * CLass use to reprensent a contact
     * @param externalOrganism ExternalOrganism Object
     * @param phoneNumber phone number as String
     * @param mailAddress mail address as String
     * @param organismState OrganismState enum
     */
    public Contact(ExternalOrganism externalOrganism, String phoneNumber, String mailAddress, OrganismState organismState) {
        this.organization.set(externalOrganism);
        this.phoneNumber.set(phoneNumber);
        this.mailAddress.set(mailAddress);
        this.state.set(organismState);
    }

    /**
     * return the organization linked to the contact
     * @return external organism
     */
    public ExternalOrganism getOrganization() {
        return organization.get();
    }

    /*
     * return the organization linked to the contact
     * @return SimpleObjectProperty<ExternalOrganism>
     */
  /*  public SimpleObjectProperty<ExternalOrganism> organizationProperty() {
        return organization;
    }*/

    /**
     * set the organization field of the contact
     * @param organization ExternalOrganism Object
     */
    public void setOrganization(ExternalOrganism organization) {
        this.organization.set(organization);
    }

    /**
     * return the phone number of the contact
     * @return String
     */
    public String getPhoneNumber() {
        return phoneNumber.get();
    }

    /*
     * return the phone number of the contact
     * @return SimpleStringProperty
     */
   /* public SimpleStringProperty phoneNumberProperty() {
        return phoneNumber;
    }*/

    /**
     * set the phone number field of the contact
     * @param phoneNumber phone number as String
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.set(phoneNumber);
    }

    /**
     * return the mail adress of the contact
     * @return String
     */
    public String getMailAddress() {
        return mailAddress.get();
    }

    /*
     * return the mail adress of the contact
     * @return SimpleStringProperty
     *//*
    public SimpleStringProperty mailAddressProperty() {
        return mailAddress;
    }*/

    /**
     * set the mail address value of the contact
     * @param mailAddress mail address as String
     */
    public void setMailAddress(String mailAddress) {
        this.mailAddress.set(mailAddress);
    }

    /**
     * return the state of the organism linked to the contact
     * @return OrganismState
     */
    public OrganismState getState() {
        return state.get();
    }

    /*
     * return the state of the organism linked to the contact
     * @return
     *//*
    public SimpleObjectProperty<OrganismState> stateProperty() {
        return state;
    }*/

    /**
     * set the state field of the organism
     * @param organismState OrganismState enum
     */
    public void setState(OrganismState organismState) {
        this.state.set(organismState);
    }


    /**
     * return string translation of the contact
     * @return String
     */
    @Override
    public String toString(){
        return String.format("%sPhone number: %s\nMail address: %s\nOrganismState: %s\n",organization.get(),phoneNumber.get(),
                mailAddress.get(),state.get()) ;
    }
}
