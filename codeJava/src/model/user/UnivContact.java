package model.user;

import model.tool.OrganismState;

public class UnivContact extends Contact {
    public UnivContact(ExternalOrganism externalOrganism, String phoneNumber, String mailAddress, OrganismState organismState) {
        super(externalOrganism, phoneNumber, mailAddress, organismState);
    }
}
