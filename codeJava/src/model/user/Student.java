package model.user;

import javafx.beans.property.SimpleObjectProperty;

/**
 * Class used to represent the student function
 */
public class Student extends Pupil {
    private final SimpleObjectProperty<Sector> sector = new SimpleObjectProperty<>(this, "sector"); //voir MaBibliothèque
/*
    public Sector getSector() {
        return sector.get();
    }*/
    /*
    public SimpleObjectProperty<Sector> sectorProperty() {
        return sector;
    }*/
/*
    public void setSector(Sector sector) {
        this.sector.set(sector);
    }*/

    /**
     * Student constructor
     * @param graduationYear graduation year of the student as int(Integer)
     * @param sector sector of study as Sector Object
     */
    public Student(int graduationYear, Sector sector) {
        this.sector.set(sector);
        super.setGraduationYear(graduationYear);
    }

    @Override
    public String toString() {
        return sector.get().toString()+"and "+graduationYear.get()+" graduate." ;
    }
}


