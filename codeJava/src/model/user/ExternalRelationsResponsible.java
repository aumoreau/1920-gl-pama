package model.user;

/**
 * Class used to represent the external organization relationship responsible
 */
public class ExternalRelationsResponsible extends Administration {
    public ExternalRelationsResponsible() {
    }

    /**
     * return the string translation of the external relation responsible
     * @return String
     */
    @Override
    public String toString(){
        return "External relations' responsible" ;
    }
}
