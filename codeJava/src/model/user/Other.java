package model.user;

import javafx.beans.property.SimpleStringProperty;

/**
 * Class used to represent all other function unrelated to the project or unknown
 */
public class Other implements Role {
    private final SimpleStringProperty function = new SimpleStringProperty(this, "function");

    /**
     * CLass use to reprensent other roles
     * @param function String
     */
    public Other(String function){
        this.function.set(function);
    }

    /*
    public Other(){
        function.set("Unregistred function");
    }*/

    /*
     * return the function of the role
     * @return String
     *//*
    public String getFunction() {
        return function.get();
    }*/

    /*
     * return the function of the role
     * @return SimpleStringProperty
     *//*
    public SimpleStringProperty functionProperty() {
        return function;
    }*/

    /*
     * set the function field of the other role
     * @param function String
     *//*
    public void setFunction(String function) {
        this.function.set(function);
    }*/

    /**
     * return the string translation of the othe role
     * @return String
     */
    @Override
    public String toString(){
        return String.format("%s\nUnimplemented function: ", function.get()) ;
    }
}
