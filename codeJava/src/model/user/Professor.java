package model.user;

import javafx.beans.property.SimpleStringProperty;

/**
 * Class used to represent a professor
 */
public class Professor extends Administration {
    private final SimpleStringProperty fields = new SimpleStringProperty(this, "fields");
    public Professor() {
        fields.set("");
    }

    /*
     * CLass use to reprensent a professor
     * @param field Professor Object
     *//*
    public Professor(String field) {
        this.fields.set(field);
    }*/

    /*
     * set the fields list field of the professor
     * @param field String
     *//*
    public void setFields(String field){
        fields.set(fields.get()+", "+field);
    }*/

    /*
     * return the list of fields of the professor
     * @return String
     *//*
    public String getFields() {
        return fields.get();
    }*/

    /*
     * return the list of fields of the professor
     * @return SimpleStringProperty
     *//*
    public SimpleStringProperty fieldsProperty() {
        return fields;
    }*/

    /**
     * return the string translation of the professor
     * @return String
     */
    @Override
    public String toString(){
        if (fields.get().length()<2){
            return "Professor\n" ;
        }else{
            return String.format("Professor in %s.\n",fields) ;
        }
    }
}
