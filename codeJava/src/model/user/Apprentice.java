package model.user;


/**
 * Class used to represent the apprentice function
 */
public class Apprentice extends Pupil {
    /**
     * Apprentice constructor
     * @param graduationYear graduation year as int (Integer)
     */
    public Apprentice(int graduationYear) {
        super.setGraduationYear(graduationYear);
    }

    /**
     * return the string translation of an apprentice
     * @return String
     */
    @Override
    public String toString(){
        return String.format("Apprentice and %d graduate.\n", graduationYear.get());
    }
}
