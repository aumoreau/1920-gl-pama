package view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.admin.Contract;
import model.tool.Grade;
import sessionhandler.Authorization;
import sessionhandler.Session;

public class GradeController extends Controller {
    @FXML
    private TextField workGrade;
    @FXML
    private TextField thesisGrade;
    @FXML
    private TextField reportGrade;
    private Contract contract;
    private  Grade grade;

    public void init(Stage stage, Grade grade, Contract contract)
    {
        setStage(stage);
        this.grade=grade;
        this.contract=contract;
        Authorization authorization = Session.getSession().getAuthorization();
        if (authorization!= Authorization.technicalOffice && authorization!=Authorization.contact)
        {
            workGrade.setDisable(true);
        }
        if (authorization!=Authorization.technicalOffice && authorization!=Authorization.departementOfEducation)
        {
            reportGrade.setDisable(true);
            thesisGrade.setDisable(true);
        }
        if (grade.getWorkMark()==-1)
        {
            workGrade.setText("");
        }
        else {
            workGrade.setText(Integer.toString(grade.getWorkMark()));
        }
        if (grade.getThesisMark()==-1)
        {
            thesisGrade.setText("");
        }
        else {
            thesisGrade.setText(Integer.toString(grade.getThesisMark()));
        }
        if (grade.getReportMark()==-1) {
            reportGrade.setText("");
        }
        else {
            reportGrade.setText(Integer.toString(grade.getReportMark()));
        }
    }

    public void handlerConfirmButton()
    {
        try {
            Integer work=null;
            Integer thesis=null;
            Integer report=null;
            if (workGrade.getText()!=null && workGrade.getText().length()!=0) {
                work = Integer.valueOf(workGrade.getText());
            }
            if (thesisGrade.getText()!=null && thesisGrade.getText().length()!=0) {
                thesis = Integer.valueOf(thesisGrade.getText());
            }
            if (reportGrade.getText()!=null && reportGrade.getText().length()!=0) {
                report = Integer.valueOf(reportGrade.getText());
            }
            if ((work!=null && 0>work)||(thesis!=null && 0>thesis)||(report!=null && 0>report)||(work!=null && 20<work)||(thesis!=null && 20<thesis)||(report!=null && 20<report))
            {
                throw new Exception();
            }
            if (workGrade.getText() != null && workGrade.getText().length() != 0) {
                grade.setWorkMark(Integer.parseInt(workGrade.getText()));
            }
            else
            {
                grade.setWorkMark(-1);
            }
            if (thesisGrade.getText() != null && thesisGrade.getText().length() != 0) {
                grade.setThesisMark(Integer.parseInt(thesisGrade.getText()));
            }else
            {
                grade.setThesisMark(-1);
            }
            if (reportGrade.getText() != null && reportGrade.getText().length() != 0) {
                grade.setReportMark(Integer.parseInt(reportGrade.getText()));
            }
            else
            {
                grade.setReportMark(-1);
            }
            Session.getSession().updateGrade(grade,contract.getContractID());
            getStage().close();
        } catch (Exception e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
            alert.setTitle("Erreur de champ");
            alert.setHeaderText(null);
            alert.setContentText("Les notes doivent être des entiers entre 0 et 20.");
            alert.showAndWait();
        }

    }

    public void handlerCancelButton()
    {
        getStage().close();
    }
}
