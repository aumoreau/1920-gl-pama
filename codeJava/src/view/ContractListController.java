package view;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.admin.Contract;
import model.user.Contact;
import model.user.Person;
import objectmanager.register.Register;
import sessionhandler.Authorization;
import sessionhandler.Session;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class ContractListController extends Controller {

    @FXML
    private TextField search;
    @FXML
    private Label type;
    @FXML
    private Label beginDate;
    @FXML
    private Label endDate;
    @FXML
    private Label workGrade;
    @FXML
    private Label reportGrade;
    @FXML
    private Label thesisGrade;
    @FXML
    private Label id;
    @FXML
    private Hyperlink organism;
    @FXML
    private Hyperlink contact;
    @FXML
    private Label student;
    @FXML
    private TableView<Contract> contractTableView;
    @FXML
    private TableColumn<Contract,String> organismColumn;
    @FXML
    private TableColumn<Contract,String> studentColumn;
    @FXML
    private TableColumn<Contract,String> stateColumn;
    @FXML
    private Button gradeButton;

    public void init(String string)
    {
        search.setText(string);
        organismColumn.setCellValueFactory(cellData->cellData.getValue().getOrganism().nameProperty());
        studentColumn.setCellValueFactory(cellData -> cellData.getValue().getPupil().lastNameProperty());
        stateColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getState().toString()));
        contractTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showDetails(newValue));
        ObservableMap<Integer,Contract> map = Register.getRegister().getObservableContract();
        ObservableList<Contract> list = FXCollections.observableArrayList(map.values());
        FilteredList<Contract> filteredList=new FilteredList<>(list,p->true);
        search.textProperty().addListener((observable, oldValue, newValue) -> filteredList.setPredicate(contract -> {
            // If filter text is empty, display all contracts
            if (newValue == null || newValue.isEmpty()) {
                return true;
            }

            String lowerCaseFilter = newValue.toLowerCase();

            Person person=contract.getContact();

            if (person.getFirstName().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else if (person.getLastName().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else if (contract.getType().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else if (contract.getPupil().getFirstName().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else if (contract.getPupil().getLastName().toLowerCase().contains(lowerCaseFilter)) {
                return true;
           } else if (contract.getOrganism().getName().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else if (contract.getGrade().toString().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else return contract.getPeriod().toString().toLowerCase().contains(lowerCaseFilter);
            // Does not match.
        }));
        SortedList<Contract> sortedList=new SortedList<>(filteredList);
        organismColumn.setSortType(TableColumn.SortType.ASCENDING);
        sortedList.comparatorProperty().bind(contractTableView.comparatorProperty());
        contractTableView.setItems(sortedList);
        contractTableView.getSortOrder().add(organismColumn);
        Authorization authorization=Session.getSession().getAuthorization();
        if (authorization!=Authorization.departementOfEducation && authorization!=Authorization.technicalOffice && authorization!=Authorization.contact)
        {
            gradeButton.setDisable(true);
            gradeButton.setVisible(false);
        }
        showDetails(null);
    }

    private void showDetails(Contract contract)
    {
        if (contract==null)
        {
            type.setText("");
            beginDate.setText("");
            endDate.setText("");
            workGrade.setText("");
            thesisGrade.setText("");
            reportGrade.setText("");
            id.setText("");
            organism.setText("");
            contact.setText("");
            student.setText("");
        }
        else
        {
            Contact contactObject=contract.getContact().getContact();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String strStartDate= formatter.format(contract.getPeriod().getStartDate());
            String strEndDate=formatter.format(contract.getPeriod().getEndDate());
            type.setText(contract.getType());
            beginDate.setText(strStartDate);
            endDate.setText(strEndDate);
            if (contract.getGrade().getWorkMark()==-1)
            {
                workGrade.setText("Non attribuée");
            }
            else {
                workGrade.setText(Integer.toString(contract.getGrade().getWorkMark()));
            }
            if (contract.getGrade().getThesisMark()==-1)
            {
                thesisGrade.setText("Non attribuée");
            }
            else {
                thesisGrade.setText(Integer.toString(contract.getGrade().getThesisMark()));
            }
            if (contract.getGrade().getReportMark()==-1) {
                reportGrade.setText("Non attribuée");
            }
            else {
                reportGrade.setText(Integer.toString(contract.getGrade().getReportMark()));
            }
            id.setText(String.valueOf(contract.getContractID()));
            contactObject.getOrganization();
            contactObject.getOrganization().getName();
            organism.setText(contactObject.getOrganization().getName());
            contact.setText(contract.getContact().getFirstName()+" "+contract.getContact().getLastName());
            student.setText(contract.getPupil().getFirstName()+" "+contract.getPupil().getLastName());
        }
    }

    public void handlerGradeButton()
    {
        if (contractTableView.getSelectionModel().getSelectedIndex()>=0) {
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(getStage());
            try {
                getMainApp().initGrade(stage, contractTableView.getSelectionModel().getSelectedItem());
            } catch (IOException e) {
                e.printStackTrace();
            }
            showDetails(contractTableView.getSelectionModel().getSelectedItem());
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
            alert.setTitle("Pas de sélection");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez sélectionner un contrat à noter");
            alert.showAndWait();
        }
    }

    public void handlerButtonCancel() {
        if (this.getStage()==getMainApp().getPrimaryStage()) {
            try {
                getMainApp().initMenu(false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
        {
            getStage().close();
        }
    }

    public void handlerContactHyperlink()
    {
        if (contractTableView.getSelectionModel().getSelectedIndex()>=0)
        {
            try {
                Stage stage = new Stage();
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(getStage());
                stage.setTitle("Vue du contact");
                stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
                getMainApp().initContactList(contractTableView.getSelectionModel().getSelectedItem().getContact(),stage,null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void handlerOrganismHyperlink()
    {
        if (contractTableView.getSelectionModel().getSelectedIndex()>=0)
        {
            try {
                Stage stage = new Stage();
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(getStage());
                stage.setTitle("Vue de l'organisme");
                stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
                getMainApp().initExteriorOrganismList(contractTableView.getSelectionModel().getSelectedItem().getOrganism(),stage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
