package view;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import objectmanager.tools.DefaultPasswordsException;
import sessionhandler.Authorization;
import sessionhandler.Session;

import java.io.IOException;

public class AuthentificationController extends Controller{
    @FXML
    private TextField id;
    @FXML
    private TextField password;
    @FXML
    private ImageView loadingAnimation;
    @FXML
    private Button connectButton;
    @FXML
    private Hyperlink forgottenPasswordHyperlink;

    public void handlerConnectButton()
    {
        id.setDisable(true);
        password.setDisable(true);
        connectButton.setDisable(true);
        forgottenPasswordHyperlink.setDisable(true);
        try {
            Session.getSession().logging(id.getText(), password.getText());
        } catch (DefaultPasswordsException e) {
            Stage stage=new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            try {
                getMainApp().initChangePassword(stage,password.getText());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Service<Boolean> dataLoadService = new Service<Boolean>(){
            @Override
            protected Task<Boolean> createTask() {
                return new Task<Boolean>(){
                    @Override
                    protected Boolean call() throws Exception { Session.getSession().loadData(); return true;}
                };
            }
        };
        dataLoadService.setOnSucceeded(event -> {
            try {
                getMainApp().initMenu(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        if (Session.getSession().isLogged() && Session.getSession().getAuthorization()!= Authorization.visitor)
        {
            loadingAnimation.setVisible(true);
            dataLoadService.start();
        }
        else
        {
            password.setText("");
            id.setDisable(false);
            password.setDisable(false);
            connectButton.setDisable(false);
            forgottenPasswordHyperlink.setDisable(false);
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
            alert.setTitle("Erreur d'authentification");
            alert.setHeaderText(null);
            if (Session.getSession().getAuthorization()==Authorization.visitor && Session.getSession().isLogged())
            {
                alert.setContentText("Vous n'avez pas accès à cette partie de l'application");
            }
            else {
                alert.setContentText("Identifiant ou mot de passe non reconnu");
            }
            alert.showAndWait();
        }
    }

    public void handlerForgottenPasswordHyperlink()
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
        alert.setTitle("Mot de passe oublié");
        alert.setContentText("Pour réinitialiser votre mot de passe, adressez-vous à l'administration");
        alert.setHeaderText(null);
        alert.showAndWait();
    }
}
