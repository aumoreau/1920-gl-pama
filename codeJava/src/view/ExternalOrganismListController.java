package view;

import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.user.ExternalOrganism;
import model.user.Person;
import objectmanager.register.Register;
import sessionhandler.Authorization;
import sessionhandler.Session;

import java.io.IOException;

/** This class allows the view to stay up to date. By adding listeners to each textFields, the controller can see whenever a change has occured
 * and change the view accordingly.*/
public class ExternalOrganismListController extends Controller {
    //récupération des objets mis en places dans le code FXML
    @FXML
    private Button contactButton;
    @FXML
    private Button editButton;
    @FXML
    private Button addButton;
    @FXML
    private Button addContactButton;
    @FXML
    private TextField search;
    @FXML
    private Label type;
    @FXML
    private Label name;
    @FXML
    private Label siret;
    @FXML
    private Label address;
    @FXML
    private Label city;
    @FXML
    private Label country;
    @FXML
    private Label state;
    @FXML
    private Label stateLabel;
    @FXML
    private TableView<ExternalOrganism> organizationTableView;  //entre crochets, ce qu'on met dans la table des vues
    @FXML
    private TableColumn<ExternalOrganism, String> nameColumn;    //entre crochets, l'objet qu'on lit dans la colonne suivi de comment on l'affiche
    private ObservableMap<String, ExternalOrganism> observableMap;


    private boolean getPredicate(ExternalOrganism externalOrganism, String newValue)
    {
        {
            // If filter text is empty, display all exteriorOrganisms.
            if (newValue == null || newValue.isEmpty()) {
                return true;
            }

            // Compare first name and last name of every person with filter text.
            String lowerCaseFilter = newValue.toLowerCase();

            if (externalOrganism.getName().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches first name.
            } else if (externalOrganism.getLocation().getAddress().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches the address.
            } else if (externalOrganism.getLocation().getCity().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches the city.
            } else if (externalOrganism.getLocation().getCountry().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches the country.
            } else if (externalOrganism.getSiret().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches the siret number.
            } else if (externalOrganism.getState().toString().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else return externalOrganism.getType().toLowerCase().contains(lowerCaseFilter); // Filter matches the type.
            // Does not match.
        }
    }

    /**
     * Initialises the view. Also allows filtering through the organisms.
     */
    public void init(String string) {
        search.setText(string);
        //réglage du contenu de la colonne "nom" à partir de l'objet qui est stocké dans le tableau
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        //Ajout d'un listener sur la table des vues qui affiche les détails de la nouvelle sélection lorsqu'on sélectionne une nouvelle ligne
        organizationTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if (Session.getSession().getAuthorization()== Authorization.contact)
            {
                if (Session.getSession().getPerson().getContact().getOrganization()==newValue) {
                    editButton.setDisable(false);
                    editButton.setVisible(true);
                }
                else
                {
                    editButton.setVisible(false);
                    editButton.setDisable(true);
                }
            }
            showDetails(newValue);
        });
        observableMap = Register.getRegister().getObservableOrganization(); // récupération de la map qui regroupe tous les OE.
        observableMap.addListener((MapChangeListener<String, ExternalOrganism>) change -> update());
        ObservableList<ExternalOrganism> observableList = FXCollections.observableArrayList(observableMap.values());// transition d'une map à une liste.
        FilteredList<ExternalOrganism> filteredList = new FilteredList<>(observableList, p -> true); // filtrage de la liste.
        //Cette partie de la méthode permet la recherche via la barre de recherche en ajoutant un listener. Elle vérifie quels sont les organismes qui vérifient encore le critère de la recherche.
        search.textProperty().addListener((observable, oldValue, newValue) -> filteredList.setPredicate(externalOrganism -> getPredicate(externalOrganism,newValue)));
        filteredList.setPredicate(externalOrganism -> getPredicate(externalOrganism,search.getText()));
        SortedList<ExternalOrganism> sortedList = new SortedList<>(filteredList);
        nameColumn.setSortType(TableColumn.SortType.ASCENDING);
        sortedList.comparatorProperty().bind(organizationTableView.comparatorProperty());
        organizationTableView.setItems(sortedList);
        organizationTableView.getSortOrder().add(nameColumn);
        Authorization authorization=Session.getSession().getAuthorization();
        if (authorization!=Authorization.externalRelationsResponsible && authorization!=Authorization.technicalOffice)
        {
            addButton.setVisible(false);
            addContactButton.setVisible(false);
            editButton.setVisible(false);
            addContactButton.setDisable(true);
            addButton.setDisable(true);
            editButton.setDisable(true);
        }
        if (authorization==Authorization.contact)
        {
            state.setVisible(false);
            state.setDisable(true);
            stateLabel.setVisible(false);
        }
        showDetails(null);
    }

    private void update()
    {
        ObservableList<ExternalOrganism> observableList = FXCollections.observableArrayList(observableMap.values());// transition d'une map à une liste.
        FilteredList<ExternalOrganism> filteredList = new FilteredList<>(observableList, p -> true); // filtrage de la liste.
        //Cette partie de la méthode permet la recherche via la barre de recherche en ajoutant un listener. Elle vérifie quels sont les organismes qui vérifient encore le critère de la recherche.
        search.textProperty().addListener((observable, oldValue, newValue) -> filteredList.setPredicate(externalOrganism -> getPredicate(externalOrganism,newValue)));
        SortedList<ExternalOrganism> sortedList = new SortedList<>(filteredList);
        sortedList.comparatorProperty().bind(organizationTableView.comparatorProperty());
        organizationTableView.setItems(sortedList);
    }

    /**
     * Simply shows the details of the ExternalOrganism given as an parameter.
     */
    public void showDetails(ExternalOrganism organization) {
        //pour vider les champs
        if (organization == null) {
            type.setText("");
            name.setText("");
            siret.setText("");
            address.setText("");
            city.setText("");
            country.setText("");
            state.setText("");
            contactButton.setDisable(true);
        }
        //pour remplir les champs
        else {
            type.setText(organization.getType());
            name.setText(organization.getName());
            siret.setText(organization.getSiret());
            address.setText(organization.getLocation().getAddress());
            city.setText(organization.getLocation().getCity());
            country.setText(organization.getLocation().getCountry());
            state.setText(organization.getState().toString());
            contactButton.setDisable(false);
        }
    }

    /**
     * This method allows canceling, and going back to the previous menu.
     */
    public void handlerCancelButton() {
        if (this.getStage()==getMainApp().getPrimaryStage()) {
            try {
                getMainApp().initMenu(false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            getStage().close();
        }
    }
    public void handlerButtonContact()
    {
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(getStage());
        stage.setTitle("Vue des contacts");
        stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
        try {
            getMainApp().initContactList(null,stage,organizationTableView.getSelectionModel().getSelectedItem());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handlerAddButton()
    {
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(getStage());
        stage.setTitle("Nouvel organisme");
        stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
        ExternalOrganism externalOrganism = null;
        try {
            getMainApp().initAddExternalOrganism(stage, externalOrganism,this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handlerAddContactButton()
    {
        if (organizationTableView.getSelectionModel().getSelectedIndex()>=0) {
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(getStage());
            stage.setTitle("Nouveau contact");
            stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
            Person person = new Person();
            ExternalOrganism externalOrganism = organizationTableView.getSelectionModel().getSelectedItem();
            try {
                getMainApp().initAddContact(stage, person, externalOrganism);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Pas de sélection");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez sélectionner une organisation à laquelle rattacher votre contact");
            alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
            alert.showAndWait();
        }
    }

    public TextField getSearch() {
        return search;
    }

    public void handlerEditButton() //à modifier
    {
        if (organizationTableView.getSelectionModel().getSelectedIndex()>=0) {
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(getStage());
            stage.setTitle("Édition d'un organisme");
            stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
            ExternalOrganism externalOrganism = organizationTableView.getSelectionModel().getSelectedItem();
            try {
                getMainApp().initAddExternalOrganism(stage,externalOrganism,this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Pas de sélection");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez sélectionner une organisation à modifier");
            alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
            alert.showAndWait();
        }
    }

    public TableView<ExternalOrganism> getOrganizationTableView() {
        return organizationTableView;
    }
}
