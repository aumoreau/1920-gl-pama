package view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.tool.OrganismState;
import model.user.Contact;
import model.user.ExternalOrganism;
import model.user.Person;
import model.user.Role;
import sessionhandler.Authorization;
import sessionhandler.Session;

import java.sql.SQLException;

public class AddContactController extends Controller{
    private Contact contact;
    private Person person;
    private ExternalOrganism externalOrganism;
    @FXML
    private Label organism;
    @FXML
    private Label stateLabel;
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField phone;
    @FXML
    private TextField mail;
    @FXML
    private ComboBox<OrganismState> state;

    public void init(Stage stage, Person person, ExternalOrganism externalOrganism)
    {
        this.person=person;
        this.setStage(stage);
        if (person.getFirstName()!=null) {
            for (Role role : person.getRoles()) {
                if (role instanceof Contact) {
                    contact = (Contact) role;
                }
            }
        }
        if (person.getFirstName()!=null)
        {
            firstName.setText(person.getFirstName());
            lastName.setText(person.getLastName());
            phone.setText(contact.getPhoneNumber());
            mail.setText(contact.getMailAddress());
            state.getSelectionModel().select(contact.getState());
        }
        this.externalOrganism=externalOrganism;
        state.getItems().addAll(OrganismState.operative, OrganismState.inoperative, OrganismState.blacklisted);
        organism.setText(externalOrganism.getName());
        if(Session.getSession().getAuthorization()== Authorization.contact)
        {
            state.setDisable(true);
            stateLabel.setVisible(false);
            state.setVisible(false);
        }
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public void handlerConfirmButton()
    {
        String error="";
        if (firstName.getText().length()==0)
        {
            error+="Remplir le champ \"Prénom\"\n";
        }
        if (lastName.getText().length()==0)
        {
            error+="Remplir le champ \"Nom\"\n";
        }
        if ((phone.getText()==null || phone.getText().length()==0) && (mail.getText()==null || mail.getText().length()==0))
        {
            error+="Remplir le champ \"Numéro de téléphone\" ou le champ \"Adresse mail\"\n";
        }
        if (mail.getText()!=null && mail.getText().length()>0 && !(isValidEmailAddress(mail.getText())))
        {
            System.out.println(mail.getText());
            error+="Mauvais format d'adresse mail\n";
        }
        if (state.getSelectionModel().getSelectedIndex()<0)
        {
            error+="Sélectionner un état";
        }

        if (error.equals("")) {
            boolean isNew = false;
            if (person.getFirstName() == null) {
                isNew = true;
                contact = new Contact();
            }
            person.setFirstName(firstName.getText());
            person.setLastName(lastName.getText());
            contact.setMailAddress(mail.getText());
            contact.setOrganization(externalOrganism);
            contact.setPhoneNumber(phone.getText());
            contact.setState(state.getValue());
            if (isNew) {
                try {
                    Session.getSession().addContactToDatabase(lastName.getText(), firstName.getText(), externalOrganism, phone.getText(), mail.getText(), state.getValue());
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
            else
            {
                Session.getSession().updateContact(person);
            }
            getStage().close();
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Erreur d'ajout");
            alert.setHeaderText(null);
            alert.setContentText(error);
            alert.showAndWait();
        }
    }

    public void handlerCancelButton()
    {
        getStage().close();
    }
}
