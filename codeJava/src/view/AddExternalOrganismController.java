package view;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.admin.Firm;
import model.study.University;
import model.tool.Location;
import model.tool.OrganismState;
import model.user.ExternalOrganism;
import objectmanager.register.Register;
import sessionhandler.Authorization;
import sessionhandler.Session;

import java.sql.SQLException;

public class AddExternalOrganismController extends Controller{
    private ExternalOrganism externalOrganism;
    @FXML
    private ComboBox<String> type;
    @FXML
    private TextField name;
    @FXML
    private TextField siret;
    @FXML
    private TextField country;
    @FXML
    private TextField city;
    @FXML
    private TextArea adress;
    @FXML
    private Label stateLabel;
    @FXML
    private ComboBox<OrganismState> state;
    private ExternalOrganismListController parentController;

    public void init(Stage stage, ExternalOrganism externalOrganism, ExternalOrganismListController parentController)
    {
        setStage(stage);
        this.externalOrganism=externalOrganism;
        this.parentController=parentController;
        state.getItems().addAll(OrganismState.operative, OrganismState.inoperative, OrganismState.blacklisted);
        type.getItems().addAll("Entreprise","Université");
        if (this.externalOrganism!=null)
        {
            if (this.externalOrganism.getType().equals("Entreprise"))
            {
                type.getSelectionModel().select(0);
            }
            else
            {
                type.getSelectionModel().select(1);
                siret.setDisable(true);
            }
            type.setDisable(true);
            name.setText(externalOrganism.getName());
            siret.setText(externalOrganism.getSiret());
            country.setText(externalOrganism.getLocation().getCountry());
            city.setText(externalOrganism.getLocation().getCity());
            adress.setText(externalOrganism.getLocation().getAddress());
            state.getSelectionModel().select(externalOrganism.getState());
            if(Session.getSession().getAuthorization()== Authorization.contact)
            {
                state.setDisable(true);
                state.setVisible(false);
                stateLabel.setVisible(false);
            }
        }
    }

    public void handlerConfirmButton()
    {
        String error="";
        if (type.getSelectionModel().getSelectedIndex()<0)
        {
            error+="Selectionner un type d'organisme\n";
        }
        if (name.getText().length()==0)
        {
            error+="Enter un nom pour l'organisme\n";
        }
        if (country.getText().length()==0 | country.getText().length()==0 | city.getText().length()==0)
        {
            error+="Remplir l'adresse entièrement\n";
        }
        if (state.getSelectionModel().getSelectedIndex()<0)
        {
            error+="Définir un état pour l'organisation\n";
        }
        if (type.getSelectionModel().getSelectedItem()=="Entreprise" && (siret.getText()==null || siret.getText().length()<=0))
        {
            error+="Veuillez remplir le champ \"S.I.R.E.T\"";
        }
        if (error.length()==0) {
            if (externalOrganism == null) {
                if (type.getSelectionModel().getSelectedItem().equals("Université")) {
                    externalOrganism = new University();
                } else {
                    externalOrganism = new Firm();
                    ((Firm) externalOrganism).setSiret(siret.getText());
                }
                externalOrganism.setLocation(new Location(country.getText(), city.getText(), adress.getText()));
                externalOrganism.setName(name.getText());
                externalOrganism.setState(state.getSelectionModel().getSelectedItem());
                Register.getRegister().addOrganization(externalOrganism);
                Session.getSession().addOrgaToDatabase(externalOrganism);
            } else {
                externalOrganism.locationProperty().setValue(new Location(country.getText(), city.getText(), adress.getText()));
                externalOrganism.setName(name.getText());
                externalOrganism.setState(state.getSelectionModel().getSelectedItem());
                if (type.getSelectionModel().getSelectedItem().equals("Entreprise")) {
                    ((Firm) externalOrganism).setSiret(siret.getText());
                }
                Session.getSession().updateOrganism(externalOrganism);
            }
            parentController.getOrganizationTableView().getSelectionModel().select(externalOrganism);
            parentController.showDetails(externalOrganism);
            getStage().close();
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
            alert.setTitle("Erreur d'ajout");
            alert.setHeaderText(null);
            alert.setContentText(error);
            alert.showAndWait();
        }
    }

    public void handlerChangeType()
    {
        if(type.getSelectionModel().getSelectedItem().equals("Université"))
        {
            siret.setText("Pas de S.I.R.E.T");
            siret.setDisable(true);
            name.setPromptText("ex : E.N.S.S.A.T");
        }
        else
        {
            siret.setText("");
            siret.setDisable(false);
            name.setPromptText("ex : Google");
        }
    }

    public void handlerCancelButton()
    {
        getStage().close();
    }
}