package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.user.Contact;
import model.user.Person;
import objectmanager.register.Register;
import sessionhandler.Authorization;
import sessionhandler.Session;
import sessionhandler.SessionType;

import java.io.IOException;

/** This class is the controller of the Contact List view. It allows it to be kept up to date at every instant. */
public class ContactListController extends Controller {
    @FXML
    private Button editButton;
    @FXML
    private TextField search;
    @FXML
    private Label lastName;
    @FXML
    private Label firstName;
    @FXML
    private Label phone;
    @FXML
    private Label mail;
    @FXML
    private Hyperlink company;
    @FXML
    private Label state;
    @FXML
    private Label stateLabel;
    @FXML
    private TableView<Person> contactTableView;
    @FXML
    private TableColumn<Person, String> contactOrganismColumn;
    @FXML
    private TableColumn<Person, String> contactFirstNameColumn;
    @FXML
    private TableColumn<Person, String> contactLastNameColumn;

    private boolean getPredicate(Person person, String newValue)
    {
        // If filter text is empty, display all exteriorOrganisms.
        if (newValue == null || newValue.isEmpty()) {
            return true;
        }

        // Compare first name and last name of every person with filter text.
        String lowerCaseFilter = newValue.toLowerCase();

        if (person.getFirstName().toLowerCase().contains(lowerCaseFilter)) {
            return true; // Filter matches first name.
        } else if (person.getLastName().toLowerCase().contains(lowerCaseFilter)) {
            return true; // Filter matches last name.
        }
        else if ((person.getUserID()+"").toLowerCase().contains(lowerCaseFilter))
        {
            return true;
        }
        Contact contact=person.getContact();
        if (contact.getOrganization().getName().toLowerCase().contains(lowerCaseFilter)) {
            return true; // Filter matches last name.
        }
        else if (contact.getMailAddress()!=null && contact.getMailAddress().toLowerCase().contains(lowerCaseFilter)) {
            return true; // Filter matches last name.
        }
        else if (contact.getPhoneNumber()!=null && contact.getPhoneNumber().toLowerCase().contains(lowerCaseFilter)) {
            return true; // Filter matches last name.
        }
        else return contact.getState().toString().toLowerCase().contains(lowerCaseFilter); // Filter matches last name.

        // Does not match.
    }

    public void init(String string) {
        search.setText(string);
        contactOrganismColumn.setCellValueFactory(cellData -> cellData.getValue().getContact().getOrganization().nameProperty());
        contactLastNameColumn.setCellValueFactory((cellData -> cellData.getValue().lastNameProperty()));
        contactFirstNameColumn.setCellValueFactory((cellData -> cellData.getValue().firstNameProperty()));
        contactTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            showDetails(newValue);
            if (Session.getSession().getSessionType()==SessionType.Contact) {
                if (Session.getSession().getPerson() == newValue)
                {
                    editButton.setVisible(true);
                    editButton.setDisable(false);
                }
                else
                {
                    editButton.setDisable(true);
                    editButton.setVisible(false);
                }
            }
        });
        ObservableList<Person> personList = FXCollections.observableArrayList(Register.getRegister().getAllContacts());
        FilteredList<Person> filteredList = new FilteredList<>(personList);
        search.textProperty().addListener((observable, oldValue, newValue) -> filteredList.setPredicate(person -> getPredicate(person,newValue)));
        filteredList.setPredicate(person -> getPredicate(person,string));
        SortedList<Person> sortedList = new SortedList<>(filteredList);
        contactOrganismColumn.setSortType(TableColumn.SortType.ASCENDING);
        sortedList.comparatorProperty().bind(contactTableView.comparatorProperty());
        contactTableView.setItems(sortedList);
        contactTableView.getSortOrder().add(contactOrganismColumn);
        Authorization authorization=Session.getSession().getAuthorization();
        if (!(authorization==Authorization.technicalOffice||authorization==Authorization.externalRelationsResponsible))
        {
            editButton.setDisable(true);
            editButton.setVisible(false);
        }
        if (Session.getSession().getAuthorization()== Authorization.contact)
        {
            state.setVisible(false);
            stateLabel.setVisible(false);
        }
        showDetails(null);
    }

    public void showDetails(Person person) {
        if (person == null) {
            firstName.setText("");
            lastName.setText("");
            phone.setText("");
            mail.setText("");
            company.setText("");
            state.setText("");
        } else {
            Contact contact=person.getContact();
            firstName.setText(person.getFirstName());
            lastName.setText((person.getLastName()));
            mail.setText(contact.getMailAddress());
            company.setText(contact.getOrganization().getName());
            phone.setText(contact.getPhoneNumber());
            state.setText(contact.getState().toString());
        }
    }

    public void handlerButtonCancel() {
        if (this.getStage()==getMainApp().getPrimaryStage()) {
            try {
                getMainApp().initMenu(false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
        {
            getStage().close();
        }
    }

    public void handlerEditButton()
    {
        if (contactTableView.getSelectionModel().getSelectedIndex()>=0) {
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(getStage());
            stage.setTitle("Édition d'un contact");
            stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
            Person person = contactTableView.getSelectionModel().getSelectedItem();
            Contact contact = person.getContact();
            try {
                getMainApp().initAddContact(stage, person, contact.getOrganization());
                showDetails(contactTableView.getSelectionModel().getSelectedItem());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
            alert.setTitle("Pas de sélection");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez sélectionner un contact");
            alert.showAndWait();
        }
    }

    public void handlerCompanyHyperlink()
    {
        if (contactTableView.getSelectionModel().getFocusedIndex()==-1)
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Pas de sélection");
            alert.setHeaderText(null);
            alert.setContentText("Merci de sélectionner un contact pour continuer");

            alert.showAndWait();
        }
        else {
            try {
                Stage stage = new Stage();
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(getStage());
                stage.setTitle("Vue des organisations");
                stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
                Contact contact = contactTableView.getSelectionModel().getSelectedItem().getContact();
                getMainApp().initExteriorOrganismList(contact.getOrganization(), stage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Hyperlink getCompany() {
        return company;
    }

    public TextField getSearch() {
        return search;
    }

    public TableView<Person> getContactTableView() {
        return contactTableView;
    }
}