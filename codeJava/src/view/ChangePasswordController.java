package view;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import sessionhandler.Session;

public class ChangePasswordController extends Controller {

    @FXML
    private TextField newPass;
    @FXML
    private TextField confirmPass;
    @FXML
    private Label error;
    private String passWord;

    public void init(Stage stage,String passWord) {
        setStage(stage);
        stage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
        stage.setTitle("Changement mot de passe");
        stage.setOnCloseRequest(Event::consume);
        this.passWord=passWord;
    }

    public void handlerChangeButton()
    {
        if (!(newPass.getText().equals(confirmPass.getText())))
        {
            error.setText("Les deux champs ne sont\npas identiques");
        }
        else if (newPass.getText().length()<8)
        {
            error.setText("Votre mot de passe doit faire\nau moins 8 caractères de long");
        }
        else
        {
            if (this.passWord.equals(newPass.getText()))
            {
                getMainApp().initKeepYourPassword();
            }
            Session.getSession().updatePassword(Session.getSession().getPerson(),newPass.getText());
            getStage().close();
        }
    }
}
