package view;

import application.MainApp;
import javafx.stage.Stage;

public abstract class Controller {
    private MainApp mainApp;
    private Stage stage;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    MainApp getMainApp() {
        return mainApp;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
