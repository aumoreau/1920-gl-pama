package view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import objectmanager.tools.FileManager;
import objectmanager.tools.FilePath;
import sessionhandler.Session;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class MenuController extends Controller {

    @FXML
    private Tooltip tooltip1,tooltip2;
    @FXML
    private Button importButton,exportButton;

    public void handlerExteriorOrganismButton() {
        try {
            getMainApp().initExteriorOrganismList(null, getMainApp().getPrimaryStage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handlerContactButton()
    {
        try {
            getMainApp().initContactList(null,getMainApp().getPrimaryStage(),null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handlerContractButton(){
        try {
            getMainApp().initContractList("",getMainApp().getPrimaryStage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handlerImportCsvButton() //handler importing from disk to model and database
    {
        DirectoryChooser directoryChooser= new DirectoryChooser();
        directoryChooser.setTitle("Choisir un fichier CSV à importer");
        File directory = directoryChooser.showDialog(new Stage());
        if (directory!=null)
        {
            FilePath.getFilePath().setFileDirectory(directory);
            FileManager.getFileManager().save();
        }
    }

    public void handlerExportCsvButton() //handler exporting from model to CSV on disk
    {
        DirectoryChooser directoryChooser=new DirectoryChooser();
        directoryChooser.setTitle("Choisir un répertoire où exporter les données");
        File directory=directoryChooser.showDialog(new Stage());
        if (directory!=null)
        {
            FilePath.getFilePath().setFileDirectory(directory);
            FileManager.getFileManager().load();
        }
    }

    public void handlerLogoutButton()
    {
        try {
            Session.getSession().endSession(false);
            getMainApp().initAuthentification();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    public void handlerEasterEggButton()
    {
        getMainApp().initEasterEgg();
    }

    public Button getImportButton() {
        return importButton;
    }

    public Button getExportButton() {
        return exportButton;
    }
}
