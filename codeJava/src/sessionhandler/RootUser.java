package sessionhandler;

import model.admin.Contract;
import model.tool.Grade;
import model.tool.OrganismState;
import model.user.ExternalOrganism;
import model.user.Person;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class used to connect to the database. It's based on singleton pattern.
 */
public class RootUser implements DatabaseInteraction{
    private static final String usernameOnline = "1920_INFO2_GroupePAMA";
    private static final String passwordOnline = "4CfgUSp93uQXuyp7PeTK4Qy8jZjZ49LY";
    private static final String urlOnline = "jdbc:mysql://barn-e-01:3306/1920_INFO2_GroupePAMA";
    private Connection connection;
    private final RequestHandler query;

    private RootUser(){
        this.connect();
        query = RequestHandler.getQuery(connection);
    }

    private static class RootUserHolder {
        private static final RootUser root = new RootUser();
    }

    /**
     * return the RootUser object
     * @return RootUser
     */
    static RootUser getRootUser() {
        return RootUserHolder.root;
    }

    void connect(){
        try {
            // connect
            Class.forName("com.mysql.jdbc.Driver");
            DriverManager.setLoginTimeout(5); //se connecter à la base locale après deux secondes sans succès
            connection = DriverManager.getConnection(urlOnline, usernameOnline, passwordOnline);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection impossible");
            //Session.databaseConnection = false;
            //connection = null ;
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    /**
     * {@link DatabaseInteraction auhenticate}
     */
    @Override
    public Person authenticate(String username, String password) throws SQLException {
        return query.authenticate(username, password);
    }


    /**
     * {@link DatabaseInteraction listAllOrganizations}
     */
    @Override
    public ArrayList<ExternalOrganism> listAllOrganizations() throws SQLException {
        return query.listAllOrganizations();
    }

    /**
     * {@link DatabaseInteraction organizationOf}
     */
    @Override
    public ExternalOrganism organizationOf(Integer userID) throws SQLException {
        return query.organizationOf(userID);
    }


    /**
     * {@link DatabaseInteraction getOrgaIdFromUserId}
     */
    @Override
    public int getOrgaIdFromUserId(Integer userId) throws SQLException {
        return query.getOrgaIdFromUserId(userId);
    }

    /**
     * {@link DatabaseInteraction listAllContactsFrom}
     */
    @Override
    public ArrayList<Person> listAllContactsFrom(Integer organismID) throws SQLException {
        return query.listAllContactsFrom(organismID);
    }

    /**
     * {@link DatabaseInteraction listAllContractsLinkedToPupil}
     */
    @Override
    public ArrayList<Contract> listAllContractsLinkedToPupil(Person pupil) throws SQLException {
        return query.listAllContractsLinkedToPupil(pupil);
    }

    /**
     * {@link DatabaseInteraction listAllContractsLinkedToContact}
     */
    @Override
    public ArrayList<Contract> listAllContractsLinkedToContact(Person contact) throws SQLException {
        return query.listAllContractsLinkedToContact(contact);
    }

    /**
     * {@link DatabaseInteraction listAllPupils}
     */
    @Override
    public ArrayList<Person> listAllPupils() throws SQLException {
        return query.listAllPupils();
    }

    /**
     * {@link DatabaseInteraction listAllContracts}
     */
    @Override
    public ArrayList<Contract> listAllContracts() throws SQLException { return query.listAllContracts(); }

    /**
     * {@link DatabaseInteraction listAllGradesLinkedToPupil}
     */
    @Override
    public ArrayList<Grade> listAllGradesLinkedToPupil(Person person) throws SQLException { return  query.listAllGradesLinkedToPupil(person); }

    /**
     * {@link DatabaseInteraction listAllGradesLinkedToContact}
     */
    @Override
    public ArrayList<Grade> listAllGradesLinkedToContact(Person person) throws SQLException { return  query.listAllGradesLinkedToContact(person); }

    /**
     * {@link DatabaseInteraction listAllGrades}
     */
    @Override
    public ArrayList<Grade> listAllGrades() throws SQLException { return  query.listAllGrades(); }

    /**
     * {@link DatabaseInteraction addContactToDatabase}
     */
    @Override
    public Person addContactToDatabase(String lastname, String firstname, ExternalOrganism organism,
                                       String phoneNumber, String mailAddress, OrganismState state) throws SQLException {
        return query.addContactToDatabase(lastname, firstname, organism,phoneNumber, mailAddress,state);
    }


    /**
     * {@link DatabaseInteraction listAllContacts}
     */
    @Override
    public ArrayList<Person> listAllContacts() throws SQLException {
        return query.listAllContacts();
    }

    /**
     * {@link DatabaseInteraction listAllUsers}
     */
    @Override
    public ArrayList<Person> listAllUsers() throws SQLException { return  query.listAllUsers(); }

    /**
     * {@link DatabaseInteraction getAuthorizationByUserId}
     */
    @Override
    public int getAuthorizationByUserId(int userId) throws SQLException { return query.getAuthorizationByUserId(userId); }

    /**
     * {@link DatabaseInteraction updateOrganism}
     */
    @Override
    public void updateOrganism(ExternalOrganism organism) {
        query.updateOrganism(organism);
    }
/*
    @Override
    public void updateUniversity(University university) {

    }

    @Override
    public void updateFirm(Firm firm) {

    }

    @Override
    public void updateContract(Contract contract) {

    }
*/
    /**
     * {@link DatabaseInteraction updateGrade}
     */
    @Override
    public void updateGrade(Grade grade,int externalOrganismID) {
        query.updateGrade(grade,externalOrganismID);
    }

    /**
     * {@link DatabaseInteraction endSession}
     */
    @Override
    public void endSession(boolean endConnection) throws SQLException {
        if(endConnection) {
            connection.close();
        }
    }

    /**
     * {@link DatabaseInteraction addOrgaToDatabase}
     */
    @Override
    public void addOrgaToDatabase(ExternalOrganism organism){
        query.addOrgaToDatabase(organism);
    }

    /**
     * {@link DatabaseInteraction updateContact}
     */
    @Override
    public void updateContact(Person person) {
        query.updateContact(person);
    }

    /**
     * {@link DatabaseInteraction updatePassword}
     */
    @Override
    public void updatePassword(Person person, String newPassword) {
        query.updatePassword(person,newPassword);
    }

    /**
     * return Connection
     * @return Connection
     */
    public Connection getConnection() {
        return connection;
    }
}
