package sessionhandler;

/**
 * Enum class used to define the type of session
 */
public enum SessionType {
    Visitor{ //En COURS
        @Override
        public String toString() {
            return "Visiteur";
        }
    },
    Student{ //En COURS
        @Override
        public String toString() {
            return "Étudiant";
        }
    },
    Contact{ //En COURS
        @Override
        public String toString() {
            return "Contact";
        }
    },
    Administration{ //En COURS
        @Override
        public String toString() {
            return "Administration";
        }
    }
}
