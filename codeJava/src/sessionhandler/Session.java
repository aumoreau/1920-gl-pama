package sessionhandler;

import model.admin.Contract;
import model.tool.Grade;
import model.tool.OrganismState;
import model.user.*;
import objectmanager.register.Register;
import objectmanager.tools.DefaultPasswordsException;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class used to manage and handle the different object. It's based on a singleton pattern
 */
public class Session implements DatabaseInteraction{
    //private static RootUser root;
    private boolean logged ;
    private final RootUser root ;
    private Person person ;
    private Authorization authorization;
    private SessionType sessionType;
    //Static
    public static Boolean databaseConnection = true ;
    //private static Session lastSession = null ;
    //private static Boolean loggedIN = false ;
    //private static int number_Of_Session = 0;

    private Session(){
        logged = false ;
        //databaseConnection = false ;
        person = null ;
        root = RootUser.getRootUser() ;
        authorization = Authorization.visitor ;
        sessionType = SessionType.Visitor ;
        //Session.loggedIN = true ;
        //Session.number_Of_Session++;
    }

    private static class SessionHolder {
        private static final Session session = new Session();
    }

    /**
     * return the Session object
     * @return Session object
     */
    public static Session getSession() {
        /*if (!loggedIN && number_Of_Session==0) {
            if (SessionHolder.session.getRootUser().getConnection() != null) {
                Session.databaseConnection = true;
            }
            Session.lastSession = SessionHolder.session ;
            return SessionHolder.session;
        }else if (!loggedIN && number_Of_Session>0){
            lastSession = new Session();
            lastSession.root.connect();
            return lastSession ;
        }else{
            return lastSession ;
        }*/
        return SessionHolder.session;
    }

    /**
     * check the credentials of the user and initialize the session if valid
     * @param username username of the user
     * @param password password of the user
     * @throws Exception when failure occurs
     */
    public void logging(String username, String password) throws Exception{
        if (root.getConnection().isClosed()){
            root.connect();
        }
        person = this.authenticate(username, password) ;
        if (person!=null ) {
            logged = true ;
            getAuthorization(person.getAuthorization());
            if (username.equals(password)){
                throw new DefaultPasswordsException("Cet utilisateur n'a pas modifié son mot de passe par défault");
            }
        } else {
            throw new Exception() ;
        }
    }

    private void getAuthorization(int authorization) {
        switch (authorization) {
            case 2:
                this.authorization = Authorization.student;
                sessionType = SessionType.Student;
                break;
            case 3:
                this.authorization = Authorization.apprentice;
                sessionType = SessionType.Student;
                break;
            case 4:
                this.authorization = Authorization.contact;
                sessionType = SessionType.Contact;
                break;
            case 5:
                this.authorization = Authorization.professor;
                sessionType = SessionType.Administration;
                break;
            case 6:
                this.authorization = Authorization.departementOfEducation;
                sessionType = SessionType.Administration;
                break;
            case 7:
                this.authorization = Authorization.externalRelationsResponsible;
                sessionType = SessionType.Administration;
                break;
            case 8:
                this.authorization = Authorization.technicalOffice;
                sessionType = SessionType.Administration;
                break;
            default:
                this.authorization = Authorization.visitor;
                sessionType = SessionType.Visitor;
                break;
        }
    }

    /**
     * {@link DatabaseInteraction getAuthorizationByUserId}
     */
    @Override
    public int getAuthorizationByUserId(int userId) throws SQLException { return  root.getAuthorizationByUserId(userId); }

    /**
     * {@link DatabaseInteraction authenticate}
     */
    @Override
    public Person authenticate(String username, String password) throws SQLException {
        return root.authenticate(username, password);
    }

    /**
     * {@link DatabaseInteraction listAllOrganizations}
     */
    @Override
    public ArrayList<ExternalOrganism> listAllOrganizations() throws SQLException {
        return root.listAllOrganizations();
    }

    /**
     * {@link DatabaseInteraction getOrgaIdFromUserId}
     */
    @Override
    public int getOrgaIdFromUserId(Integer userID) throws SQLException{
        return root.getOrgaIdFromUserId(userID);
    }

    /**
     * {@link DatabaseInteraction organizationOf}
     */
    @Override
    public ExternalOrganism organizationOf(Integer userID) throws SQLException {
        return root.organizationOf(userID);
    }

    /**
     * {@link DatabaseInteraction listAllContactsFrom}
     */
    @Override
    public ArrayList<Person> listAllContactsFrom(Integer organismID) throws SQLException {
        return root.listAllContactsFrom(organismID);
    }

    /**
     * {@link DatabaseInteraction listAllContractsLinkedToPupil}
     */
    @Override
    public ArrayList<Contract> listAllContractsLinkedToPupil(Person pupil) throws SQLException {
        return root.listAllContractsLinkedToPupil(pupil);
    }

    /**
     * {@link DatabaseInteraction listAllContractsLinkedToContact}
     */
    @Override
    public ArrayList<Contract> listAllContractsLinkedToContact(Person contact) throws SQLException {
        return root.listAllContractsLinkedToContact(contact);
    }

    /**
     * {@link DatabaseInteraction listAllContracts}
     */
    @Override
    public ArrayList<Contract> listAllContracts() throws SQLException {
        return root.listAllContracts();
    }

    /**
     * {@link DatabaseInteraction listAllPupils}
     */
    @Override
    public ArrayList<Person> listAllPupils() throws SQLException {
        return root.listAllPupils();
    }

    /**
     * {@link DatabaseInteraction listAllGradesLinkedToPupil}
     */
    @Override
    public ArrayList<Grade> listAllGradesLinkedToPupil(Person person) throws  SQLException {
        return root.listAllGradesLinkedToPupil(person);
    }

    /**
     * {@link DatabaseInteraction listAllGradesLinkedToContact}
     */
    @Override
    public ArrayList<Grade> listAllGradesLinkedToContact(Person person) throws  SQLException {
        return root.listAllGradesLinkedToContact(person);
    }

    /**
     * {@link DatabaseInteraction listAllGrades}
     */
    @Override
    public ArrayList<Grade> listAllGrades() throws  SQLException {
        return root.listAllGrades();
    }

    /**
     * {@link DatabaseInteraction updateContact}
     */
    @Override
    public void updateContact(Person person) {
        root.updateContact(person);
    }

    /**
     * {@link DatabaseInteraction addContactToDatabase}
     */
    @Override
    public Person addContactToDatabase(String lastname, String firstname, ExternalOrganism externalOrganism,
                                       String phoneNumber, String mailAddress, OrganismState state)  throws SQLException{
        return root.addContactToDatabase(lastname, firstname, externalOrganism,phoneNumber, mailAddress,state);
    }

    /**
     * {@link DatabaseInteraction addOrgaToDatabase}
     */
    @Override
    public void addOrgaToDatabase(ExternalOrganism organism) {
        root.addOrgaToDatabase(organism);
    }

    /**
     * {@link DatabaseInteraction listAllContacts}
     */
    @Override
    public ArrayList<Person> listAllContacts() throws SQLException{
        return root.listAllContacts();
    }

    /**
     * {@link DatabaseInteraction updateOrganism}
     */
    @Override
    public void updateOrganism(ExternalOrganism organism) {
        root.updateOrganism(organism);
    }

    /**
     * {@link DatabaseInteraction listAllUsers}
     */
    @Override
    public ArrayList<Person> listAllUsers() throws SQLException { return root.listAllUsers(); }

    public void loadData() throws SQLException{
        switch (sessionType) {
            case Visitor:
                listAllOrganizations();
                break;
            case Student:
                listAllOrganizations();
                listAllContacts();
                listAllPupils();
                listAllContractsLinkedToPupil(person);
                listAllGradesLinkedToPupil(person);
                break;
            case Administration:
                listAllOrganizations();
                listAllPupils();
                listAllContacts();
                listAllContracts();
                listAllGrades();
                break;
            case Contact:
                organizationOf(person.getUserID());
                listAllContactsFrom(getOrgaIdFromUserId(person.getUserID()));
                listAllPupils();
                listAllContractsLinkedToContact(person);
                listAllGradesLinkedToContact(person);
                break;
        }
    }

    /**
     * {@link DatabaseInteraction updateGrade}
     */
    @Override
    public void updateGrade(Grade grade,int externalOrganismID) { root.updateGrade(grade,externalOrganismID); }

    /**
     * {@link DatabaseInteraction updatePassword}
     */
    @Override
    public void updatePassword(Person person, String newPassword) {
        root.updatePassword(person,newPassword);
    }

    /**
     * {@link DatabaseInteraction endSession}
     */
    @Override
    public void endSession(boolean endConnection) throws SQLException {

        root.endSession(endConnection);
        logged = false ;
        person = null ;
        authorization = Authorization.visitor ;
        sessionType = SessionType.Visitor ;
        Register.getRegister().flushAllRegisters();
    }

    private RootUser getRootUser(){
        return root;
    }

    /**
     * return the login session status
     * @return boolean isLogged
     */
    public boolean isLogged() {
        return logged;
    }

    /**
     * return the type of session
     * @return SessionType enum
     */
    public SessionType getSessionType() {
        return sessionType;
    }

    /**
     * return the authorization level
     * @return Authorization level
     */
    public Authorization getAuthorization() {
        return authorization;
    }

    /**
     * return the Person logged to the session
     * @return Person
     */
    public Person getPerson() {
        return person;
    }

}