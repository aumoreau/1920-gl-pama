package sessionhandler;

import model.admin.Contract;
import model.tool.Grade;
import model.tool.OrganismState;
import model.user.Contact;
import model.user.ExternalOrganism;
import model.user.Person;
import model.user.Sector;
import objectmanager.factory.Factory;
import objectmanager.register.Register;
import objectmanager.tools.UnknownObjectException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Class used to send queries to the database. It's based on singleton pattern
 */
public class RequestHandler implements DatabaseInteraction {
    private Connection connection;
    private PreparedStatement statement;
    private final Factory factory ;
    private final DatabaseAttributes attribute ;

    private RequestHandler() {
        factory = Factory.getFactory();
        attribute = DatabaseAttributes.getDatabaseAttributes() ;
    }

    private static class RequestHandlerHolder {
        private static final RequestHandler request = new RequestHandler();
    }

    /**
     * return the RequestHandler object
     * @param con Connection object
     * @return RequestHandler object
     */
    public static RequestHandler getQuery(Connection con) {
        RequestHandler query = RequestHandlerHolder.request;
        query.setCon(con);
        return query;
    }

    /**
     * set the Connection object
     * @param con Connection object
     */
    public void setCon(Connection con) {
        connection = con;
    }

    /**
     * {@link DatabaseInteraction auhenticate}
     */
    @Override
    public Person authenticate(String username, String password) throws SQLException {
        Person person = null ;
        ResultSet result = null;
        //Connection à la session en cours
        try {
            statement = connection.prepareStatement(String.format("SELECT %s,%s,%s,%s FROM %s WHERE %s=? and %s=?",
                    attribute.idUt,attribute.authorization,attribute.identifiant,attribute.password,attribute.tableUser, attribute.identifiant,attribute.password));
            statement.setString(1, username);
            statement.setString(2, password);
            result = statement.executeQuery();
            //System.out.println(result.getFetchSize());
            while (result.next()) {
                if(!result.getString(attribute.identifiant).equals(username)||!result.getString(attribute.password).equals(password)){
                    person = null ;
                }else {
                    organizationOf(result.getInt(attribute.idUt));
                    person = this.createPerson(result.getInt(attribute.idUt));
                    person.setAuthorization(result.getInt(attribute.authorization));
                }
            }
            if(person==null){
                throw new NullPointerException("Utilisateur non identifié");
            }
            return person;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }


    private Person createPerson(int userID) throws SQLException {
        Person person = null ;
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(String.format("SELECT * FROM %s WHERE %s=?",attribute.tableUser,attribute.idUt));
            statement.setInt(1, userID);
            result = statement.executeQuery();
            while (result.next()) {
                person = factory.createPerson(
                        result.getInt(attribute.idUt),
                        result.getString(attribute.firstname),
                        result.getString(attribute.lastname),
                        result.getString(attribute.identifiant)
                );
            }
            this.identifyFunction(person);
        }catch(SQLException e){
            e.printStackTrace();
            return null ;
        }finally {
            if (result != null){
                result.close();
            }
        }
        return person ;
    }


    private void identifyFunction(Person person) throws SQLException {
        String[] table = {attribute.tableStudent,attribute.tableContact,attribute.tableStaff,
                attribute.idUt_Ele,attribute.idUt_Contact,attribute.idUt_Personnel} ;
        ResultSet result ;
        for (int i=0;i<3;i++){
            result = null ;
            try{
                statement = connection.prepareStatement(String.format("SELECT * FROM %s WHERE %s=?",table[i],table[i+3]));
                statement.setInt(1,person.getUserID());
                result = statement.executeQuery();
                switch (i) {
                    case 0:
                        while (result.next()) {
                            switch (result.getString(attribute.sector)) {
                                case "PHOTO":
                                    factory.addFunction(person, result.getString(attribute.sector), result.getInt(attribute.graduationYear), Sector.PHOTO);
                                    break;
                                case "INFO":
                                    factory.addFunction(person, result.getString(attribute.sector), result.getInt(attribute.graduationYear), Sector.INFO);
                                    break;
                                case "SNUM":
                                    factory.addFunction(person, result.getString(attribute.sector), result.getInt(attribute.graduationYear), Sector.SNUM);
                                    break;
                                case "IMR":
                                    factory.addFunction(person, result.getString(attribute.sector), result.getInt(attribute.graduationYear), Sector.NaN);
                                    break;
                            }
                        }
                        break;
                    case 1:
                        while (result.next()) {
                            factory.addFunctionContact(
                                    person,
                                    result.getInt(attribute.idOrgaContact),
                                    result.getString(attribute.phone),
                                    result.getString(attribute.mail),
                                    result.getString(attribute.etatContact)
                            );
                        }
                        break;
                    case 2:
                        while (result.next()) {
                            factory.addFunction(person, result.getString(attribute.fonction), 0, Sector.NaN);
                        }
                        break;
                }
            }catch (SQLException e){
                e.printStackTrace();
            }finally {
                if (result != null){
                    result.close();
                }
            }
        }
    }

    private ExternalOrganism createOrganism(int idOrga) throws SQLException {
        ExternalOrganism organism = Register.getRegister().searchOrganization(idOrga);
        if (organism!=null){
            return organism ;
        }
        ResultSet result = null ;
        try {
            OrganismState state ;
            statement = connection.prepareStatement(String.format("SELECT * FROM %s WHERE %s=?",attribute.tableOrgaExt,
                    attribute.idOrga));
            statement.setInt(1, idOrga);
            result = statement.executeQuery();
            while (result.next()) {
                if(result.getString(attribute.stateOrga).equals("Actif")) {
                    state = OrganismState.operative ;
                }else if(result.getString(attribute.stateOrga).equals("Inactif")){
                    state = OrganismState.inoperative ;
                }else{
                    state = OrganismState.blacklisted ;
                }
                if ("Université".equals(result.getString(attribute.siretOrga))) {
                    organism = factory.createUniversity(result.getInt(attribute.idOrga), result.getString(attribute.nameOrga), result.getString(attribute.countryOrga),
                            result.getString(attribute.cityOrga), result.getString(attribute.adressOrga), state);
                } else {
                    organism = factory.createCompany(result.getInt(attribute.idOrga), result.getString(attribute.siretOrga), result.getString(attribute.nameOrga),
                            result.getString(attribute.countryOrga), result.getString(attribute.cityOrga), result.getString(attribute.adressOrga),
                            state);
                }
            }
            return organism ;
        }catch(SQLException e){
            e.printStackTrace();
            return null ;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction listAllOrganizations}
     */
    @Override
    public ArrayList<ExternalOrganism> listAllOrganizations() throws SQLException { //revoie liste de 2 sous-listes : les entreprises et les universités
        ResultSet result = null ;
        try {
            ExternalOrganism organism ;
            ArrayList<ExternalOrganism> allOrganizations = new ArrayList<>();
            statement = connection.prepareStatement(String.format("SELECT %s FROM %s",attribute.idOrga,attribute.tableOrgaExt));
            result = statement.executeQuery();
            while (result.next()) { // en cours de parcours des lignes de la table de BDD
                organism = this.createOrganism(result.getInt(attribute.idOrga));
                if (organism!=null) {
                    allOrganizations.add(organism);
                }
            }
            return allOrganizations;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction organizationOf}
     */
    @Override
    public ExternalOrganism organizationOf(Integer userID) throws SQLException { //renvoie l'organisme externe lié au contact associé à userID
        ResultSet result = null;

        try {
            ExternalOrganism organization = null;
            //"SELECT * FROM OrganismeExt, Contact, Utilisateur WHERE idOrga_Orga = id_Orga_Cont AND idUt_Cont = idUt_Ut AND idUt_Ut = ? ");
            String requete = String.format(
                    "SELECT %s FROM %s, %s, %s WHERE %s=%s AND %s=%s AND %s= ? ",
                    attribute.idOrga,
                    attribute.tableOrgaExt,
                    attribute.tableContact,
                    attribute.tableUser,
                    attribute.idOrga,
                    attribute.idOrgaContact,
                    attribute.idUt_Contact,
                    attribute.idUt,
                    attribute.idUt
            );
            statement = connection.prepareStatement(requete);
            statement.setInt(1, userID);
            result = statement.executeQuery();
            while(result.next()) {
                organization = this.createOrganism(result.getInt(attribute.idOrga));
            }
            return organization ;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction getOrgaIdFromUserId}
     */
    @Override
    public int getOrgaIdFromUserId(Integer userID) throws SQLException { //renvoie l'organisme externe lié au contact associé à userID
        ResultSet result = null;

        try {
            int idOrga=-1;
            //"SELECT * FROM OrganismeExt, Contact, Utilisateur WHERE idOrga_Orga = id_Orga_Cont AND idUt_Cont = idUt_Ut AND idUt_Ut = ? ");
            String requete = String.format(
                    "SELECT %s FROM %s, %s WHERE %s=%s AND %s= ? ",
                    attribute.idOrgaContact,
                    attribute.tableContact,
                    attribute.tableUser,
                    attribute.idUt_Contact,
                    attribute.idUt,
                    attribute.idUt
            );
            statement = connection.prepareStatement(requete);
            statement.setInt(1, userID);
            result = statement.executeQuery();
            while(result.next()) {
                idOrga = result.getInt(1);
            }
            return idOrga ;
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction listAllContactsFrom}
     */
   @Override
    public ArrayList<Person> listAllContactsFrom(Integer organismID) throws SQLException {
        ArrayList<Person> contacts ;
        ResultSet result = null ;
        //renvoie les Personnes qui ont pour rôle "Contact" (uniquement) et qui sont liées à cet organisme
        try {
            contacts = new ArrayList<>();
            //"SELECT * FROM OrganismExt, Contact, Utilisateur WHERE idUt_Ut = idUt_Cont AND idOrgaCont = id_Orga_Orga AND idOrga_Orga = ?"
            String requete = String.format(
                    "Select %s from %s,%s,%s Where %s=%s and %s=%s and %s=?",
                    attribute.idUt,
                    attribute.tableContact,
                    attribute.tableUser,
                    attribute.tableOrgaExt,
                    attribute.idUt,
                    attribute.idUt_Contact,
                    attribute.idOrgaContact,
                    attribute.idOrga,
                    attribute.idOrga
            );
            statement = connection.prepareStatement(requete);
            statement.setInt(1, organismID);
            result = statement.executeQuery();
            while (result.next()) {
                Person a = this.createPerson(result.getInt(attribute.idUt));
                contacts.add(a);
            }
            return contacts;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction listAllContacts}
     */
    @Override
    public ArrayList<Person> listAllContacts() throws SQLException {
        ResultSet result = null;
        try {
            ArrayList<Person> allcontacts = new ArrayList<>() ;
            //"SELECT * FROM OrganismExt, Contact, Utilisateur WHERE idUt_Ut = idUt_Cont AND idOrgaCont = id_Orga_Orga"
            String requete = String.format("Select %s from %s,%s, %s Where %s=%s and %s=%s",
                    attribute.idUt,attribute.tableContact,attribute.tableUser,attribute.tableOrgaExt,
                    attribute.idUt,attribute.idUt_Contact,attribute.idOrgaContact,attribute.idOrga);
            statement = connection.prepareStatement(requete);
            result = statement.executeQuery();
            while (result.next()) {
                allcontacts.add(this.createPerson(result.getInt(attribute.idUt))) ;
            }
            return allcontacts;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /*
     * @param userId the id of the user
     * @return The ID of user as a String
     * @throws SQLException when query fails.
     *//*
    public int getPupilIdFromUserId(int userId) throws SQLException {
        ResultSet result = null ;
        try {
            //SELECT idEle_Ele FROM Eleve WHERE idUt_Ele=userID;
            statement = connection.prepareStatement(String.format(
                    "SELECT %s FROM %s WHERE idUt_Ele=?",
                    attribute.idEleve,
                    attribute.tableStudent,
                    attribute.idUt_Ele
            ));
            statement.setInt(1, userId);
            result = statement.executeQuery();
            if (result.next()) {
                return result.getInt(attribute.idEleve);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }finally {
            if (result != null){
                result.close();
            }
        }
        return 0;
    }*/

    private int getStudentIdFromUserId(int userId) throws SQLException {
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT * FROM %s",
                    attribute.tableStudent
            ));
            result = statement.executeQuery();
            while (result.next()) {
                if(result.getInt(2)==userId){
                    return result.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }finally {
            if (result != null){
                result.close();
            }
        }
        return -1;
    }

    /**
    * {@link DatabaseInteraction listAllContractsLinkedToPupil}
    */
    @Override
    public ArrayList<Contract> listAllContractsLinkedToPupil(Person pupil) throws SQLException {
        ArrayList<Contract> contracts = new ArrayList<>();
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT * FROM %s",
                    attribute.tableContract
            ));
            result = statement.executeQuery();
            while (result.next()) {
                if(((Integer) result.getInt(4)).equals(getStudentIdFromUserId(pupil.getUserID()))) {
                    contracts.add(factory.createContract(
                            result.getInt(1),
                            pupil.getUserID(),
                            this.getUserIdFromContactId(result.getInt(2)),
                            result.getInt(3),
                            result.getDate(8),
                            result.getDate(9),
                            result.getString(5),
                            result.getString(7)

                    ));
                }
            }
            return contracts;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     *  Search the user id from a contact id
     * @param contactId the id of the contact
     * @return the userId of the contact as a String
     * @throws SQLException when query fails.
     */
    private int getContactIdFromUserId(int contactId) throws SQLException {
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT %s FROM %s WHERE %s=?",
                    attribute.idContact,
                    attribute.tableContact,
                    attribute.idUt_Contact
            ));
            statement.setInt(1, contactId);
            result = statement.executeQuery();
            if (result.next()) {
                return result.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }finally {
            if (result != null){
                result.close();
            }
        }
        return -1;
    }

    /**
     * {@link DatabaseInteraction listAllContractsLinkedToContact}
     */
    @Override
    public ArrayList<Contract> listAllContractsLinkedToContact(Person contact) throws SQLException {
        ArrayList<Contract> contracts = new ArrayList<>();
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT * FROM %s",
                    attribute.tableContract
            ));
            result = statement.executeQuery();
            while (result.next()) {
                if(((Integer) result.getInt(2)).equals(getContactIdFromUserId(contact.getUserID()))){
                    contracts.add( factory.createContract(
                            result.getInt(1),
                            getUserIdFromStudentId(result.getInt(4)),
                            contact.getUserID(),
                            result.getInt(3),
                            result.getDate(8),
                            result.getDate(9),
                            result.getString(5),
                            result.getString(7)
                    ));
                }
            }
            return contracts;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction listAllUsers}
     */
    @Override
    public ArrayList<Person> listAllUsers() throws SQLException {
        ArrayList<Person> users = new ArrayList<>();
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT %s FROM %s",attribute.idUt,attribute.tableUser
            ));
            result = statement.executeQuery();
            while (result.next()) {
                users.add(this.createPerson(
                        result.getInt(attribute.idUt)));
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction listAllContracts}
     */
    @Override
    public ArrayList<Contract> listAllContracts() throws SQLException {
        ArrayList<Contract> contracts = new ArrayList<>();
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT * FROM %s",
                    attribute.tableContract
            ));
            result = statement.executeQuery();
            while (result.next()) {
                contracts.add( factory.createContract(
                        result.getInt(1),
                        getUserIdFromStudentId(result.getInt(4)),
                        getUserIdFromContactId(result.getInt(2)),
                        result.getInt(3),
                        result.getDate(8),
                        result.getDate(9),
                        result.getString(5),
                        result.getString(7)
                ));
            }
            return contracts;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction listAllPupils}
     */
    @Override
    public ArrayList<Person> listAllPupils() throws SQLException {
        ResultSet result = null ;
        try {
            String requete = String.format("SELECT %s FROM %s,%s WHERE %s=%s",
                    attribute.idUt,attribute.tableUser,attribute.tableStudent, attribute.idUt, attribute.idUt_Ele);
            ArrayList<Person> pupilsAndApprentices = new ArrayList<>();
            statement = connection.prepareStatement(requete);
            result = statement.executeQuery();
            while (result.next()) {
                pupilsAndApprentices.add(this.createPerson(result.getInt(attribute.idUt)));
            }
            return pupilsAndApprentices;
        } catch (IllegalStateException | SQLException e) {
            e.printStackTrace();
            return null ;
        } finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction addContactToDatabase}
     */
    @Override
    public Person addContactToDatabase(String lastname, String firstname, ExternalOrganism organism,
                                       String phoneNumber, String mailAddress, OrganismState state) throws SQLException {
        ResultSet result = null;
        try{
            String call = "CALL creation_contact (?,?,?,?,?,?)";
            CallableStatement procedure = connection.prepareCall(call);
            procedure.setString(1,lastname);
            procedure.setString(2,firstname);
            procedure.setString(3,organism.getName());
            procedure.setString(4, mailAddress);
            procedure.setString(5,phoneNumber);
            if (state==OrganismState.inoperative)
            {
                procedure.setString(6, "Inactif");
            }
            else if (state==OrganismState.blacklisted)
            {
                procedure.setString(6, "Blacklisté");
            }
            else
            {
                procedure.setString(6,"Actif");
            }
            procedure.executeUpdate() ;
            Person person = null ;
            String requete = String.format("Select %s from %s, %s where %s='%s' and %s='%s' and %s=%s and %s=%s",
                    attribute.idUt,attribute.tableUser,attribute.tableContact,
                    attribute.lastname,lastname,attribute.firstname,firstname,
                    attribute.idUt,attribute.idUt_Contact,
                    attribute.idOrgaContact, organism.getIdOrga());
            statement = connection.prepareStatement(requete);
            result = statement.executeQuery();
            while(result.next()){
                person = this.createPerson(result.getInt(attribute.idUt));
            }
            return person;
        } catch (SQLException e) {
            e.printStackTrace();
            return  null;
        }finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction addOrgaToDatabase}
     */
    @Override
    public void addOrgaToDatabase(ExternalOrganism organism) {
        try{
            String call = "CALL creation_organisme (?,?,?,?,?,?)";
            CallableStatement procedure = connection.prepareCall(call);
            procedure.setString(1,organism.getName());
            procedure.setString(2,organism.getLocation().getAddress());
            procedure.setString(3,organism.getLocation().getCity());
            procedure.setString(4,organism.getLocation().getCountry());
            procedure.setString(5,organism.getSiret());
            procedure.setString(6,organism.getState().toString());
            procedure.executeUpdate() ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@link DatabaseInteraction getuthorizationByUserId}
     */
    @Override
    public int getAuthorizationByUserId( int userId) throws SQLException {
        ResultSet result = null ;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT %s FROM %s WHERE %s=?",
                    attribute.authorization,
                    attribute.tableUser,
                    attribute.idUt
            ));
            statement.setInt(1, userId);
            result = statement.executeQuery();
            if (result.next()) {
                return result.getInt(attribute.authorization);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
        finally {
            if (result != null){
                result.close();
            }
        }
        return -1;
    }

    /**
     * {@link DatabaseInteraction updateContact}
     */
    @Override
    public void updateContact(Person person) {
        Contact contact = person.getContact() ;
        String state ;
        if (contact!=null){
            try {
                //call modification_contact(nom,prenom,mail,telephone,etat
                String call = "CALL modification_contact (?,?,?,?,?)";
                CallableStatement procedure = connection.prepareCall(call);
                switch(contact.getState()){
                    case operative:
                        state = "Actif";
                        break;
                    case inoperative:
                        state = "Inactif";
                        break;
                    default:
                        state = "Blacklisté";
                        break;
                }
                procedure.setString(1,person.getLastName());
                procedure.setString(2,person.getFirstName());
                procedure.setString(3,contact.getMailAddress());
                procedure.setString(4,contact.getPhoneNumber());
                procedure.setString(5,state);
                procedure.executeUpdate();
                procedure.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private int getUserIdFromStudentId(int studentID) throws SQLException {
        ResultSet result = null ;

        try {
            //SELECT  FROM Eleve WHERE idUt_Ele=userID;
            statement = connection.prepareStatement(String.format(
                    "SELECT %s FROM %s WHERE %s=?",
                    attribute.idUt_Ele,
                    attribute.tableStudent,
                    attribute.idEleve
            ));
            statement.setInt(1, studentID);
            result = statement.executeQuery();
            if (result.next()) {
                return result.getInt(attribute.idUt_Ele);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }finally {
            if (result != null){
                result.close();
            }
        }
        return 0;
    }

    private int getUserIdFromContactId(int contactID) throws SQLException {
        ResultSet result = null ;
        try {
            //SELECT idUt_Cont FROM Contact WHERE idContact_Cont=contactID;
            statement = connection.prepareStatement(String.format(
                    "SELECT * FROM %s",
                    attribute.tableContact
            ));
            result = statement.executeQuery();
            while (result.next()) {
                if(result.getInt(1)==contactID) {
                    return result.getInt(3);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }finally {
            if (result != null){
                result.close();
            }
        }
        return 0;
    }

    /**
     * {@link DatabaseInteraction listAllgradesLinkedToPupil}
     */
    @Override
    public ArrayList<Grade> listAllGradesLinkedToPupil(Person pupil) throws SQLException {
        ResultSet result = null ;
        try {
            int studentId = getStudentIdFromUserId(pupil.getUserID());
            String requete = String.format(
                    "SELECT %s, %s, %s, %s FROM %s, %s WHERE %s=%s AND %s=?",
                    attribute.idMarks,
                    attribute.idContractNote,
                    attribute.valuesMarks,
                    attribute.originMarks,
                    attribute.tableMarks,
                    attribute.tableContract,
                    attribute.idContractNote,
                    attribute.idContract,
                    attribute.idEleveContract
            );

            ArrayList<Grade> grades = new ArrayList<>();
            statement = connection.prepareStatement(requete);
            statement.setInt(1, studentId);
            result = statement.executeQuery();
            while (result.next()) {
                grades.add(factory.createGrade(result.getInt(attribute.idMarks),result.getInt(attribute.idContractNote),
                        result.getInt(attribute.valuesMarks),result.getString(attribute.originMarks)));
            }
            return grades;
        } catch (IllegalStateException | SQLException e) {
            e.printStackTrace();
            return null ;
        } finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction listAllGradesLinkedToContact}
     */
    @Override
    public ArrayList<Grade> listAllGradesLinkedToContact(Person contact) throws SQLException {
        ResultSet result = null ;
        try {
            int contactId = getContactIdFromUserId(contact.getUserID());
            String requete ;
            requete = "SELECT Note.idNote_Note, Note.idContrat_Note, Note.valeur_Note, Note.origine_Note FROM Note, Contrat WHERE Contrat.idContrat_Ctr=Note.idContrat_Note AND Contrat.idContact_Ctr = ? ";
                    //String requete = String.format(
                    /*"SELECT %s, %s, %s, %s FROM %s, %s WHERE %s=%s AND %s=?"*/
            /*        "SELECT Note.idNote_Note, Note.idContrat_Note, Note.valeur_Note, Note.origine_Note FROM Note, Contrat WHERE Contrat.idContrat_Ctr=Note.idContrat_Note AND Contrat.idContact_Ctr = ? ",
                    attribute.idMarks,
                    attribute.idContractNote,
                    attribute.valuesMarks,
                    attribute.originMarks,
                    attribute.tableMarks,
                    attribute.tableContract,
                    attribute.idContractNote,
                    attribute.idContract,
                    attribute.idContactContract
            );*/

            ArrayList<Grade> grades = new ArrayList<>();
            statement = connection.prepareStatement(requete);
            statement.setInt(1, contactId );
            result = statement.executeQuery();
            while (result.next()) {
                grades.add(factory.createGrade(result.getInt(attribute.idMarks),result.getInt(attribute.idContractNote),
                        result.getInt(attribute.valuesMarks),result.getString(attribute.originMarks)));
            }
            return grades;
        } catch (IllegalStateException | SQLException e) {
            e.printStackTrace();
            return null ;
        } finally {
            if (result != null){
                result.close();
            }
        }
    }

    /**
     * {@link DatabaseInteraction listAllGrades}
     */
    @Override
    public ArrayList<Grade> listAllGrades() throws SQLException {
        ResultSet result = null ;
        try {
            String requete = String.format(
                    "SELECT %s, %s, %s, %s FROM %s",
                    attribute.idMarks,
                    attribute.idContractNote,
                    attribute.valuesMarks,
                    attribute.originMarks,
                    attribute.tableMarks
            );
            ArrayList<Grade> grades = new ArrayList<>();
            statement = connection.prepareStatement(requete);
            result = statement.executeQuery();
            //createGrade(Integer idGrade, Integer idContract, Integer mark,String label);
            while (result.next()) {
                grades.add(factory.createGrade(result.getInt(attribute.idMarks),result.getInt(attribute.idContractNote),
                        result.getInt(attribute.valuesMarks),result.getString(attribute.originMarks)));
            }
            return grades;
        } catch (IllegalStateException | SQLException e) {
            e.printStackTrace();
            return null ;
        } finally {
            if (result != null){
                result.close();
            }
        }
    }


    /**
     * {@link DatabaseInteraction updateOrganism}
     */
    @Override
    public void updateOrganism(ExternalOrganism organism) {
        try {
            if (Register.getRegister().namedChanged(organism)) {
                this.updateNameOrga(organism);
            }
            //call modification_organism(nom,adress,ville,pays,country,siret,etat
            String call = "CALL modification_organisme (?,?,?,?,?,?)";
            String state;
            switch (organism.getState().toString()){
                case "Actif":
                    state="Actif";
                    break;
                case "Inactif":
                    state="Inactif";
                    break;
                default:
                    state="Blacklisté";
                    break;
            }
            CallableStatement procedure = connection.prepareCall(call);
            procedure.setString(1,organism.getName());
            procedure.setString(2,organism.getLocation().getAddress());
            procedure.setString(3,organism.getLocation().getCity());
            procedure.setString(4,organism.getLocation().getCountry());
            procedure.setString(5,organism.getSiret());
            procedure.setString(6,state);
            procedure.executeUpdate();
            procedure.close();
        }catch (UnknownObjectException e){
            System.out.println("Cet object est inconnu du système de registre et est null");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void updateNameOrga(ExternalOrganism organism){
        try {
            //UPDATE table
            //SET nom_colonne_1 = 'nouvelle valeur'
            //WHERE condition
            String requete = String.format("UPDATE %s SET %s=? WHERE %s=?",attribute.tableOrgaExt,
                    attribute.nameOrga,attribute.idOrga);
            statement = connection.prepareStatement(requete);
            statement.setString(1, organism.getName());
            statement.setInt(2, organism.getIdOrga());
            int res = statement.executeUpdate();
            if (res>0){
                Register.getRegister().updateNameMap(organism);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
/*
    @Override
    public void updateUniversity(University university) {

    }

    @Override
    public void updateFirm(Firm firm) {

    }
*/
/*
    public Integer getIdContractByIdGrade(Integer idGrade) {
        ResultSet result = null ;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT %s FROM %s WHERE %s=?",
                    attribute.idContractNote,
                    attribute.tableMarks,
                    attribute.idMarks
            ));
            statement.setInt(1, idGrade);
            result = statement.executeQuery();
            if (result.next()) {
                    return result.getInt(attribute.idContractNote);
            }
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
*/
    private boolean isAlreadyInGrade(Integer idContract, String origineNote) {
        ResultSet result ;
        try {
            statement = connection.prepareStatement(String.format(
                    "SELECT %s FROM %s WHERE %s=? AND %s=?",
                    attribute.idMarks,
                    attribute.tableMarks,
                    attribute.idContractNote,
                    attribute.originMarks
            ));
            statement.setInt(1, idContract);
            statement.setString(2, origineNote);
            result = statement.executeQuery();
            return result.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * {@link DatabaseInteraction updateGrade}
     */
    @Override
    public void updateGrade(Grade grade, int idContract) {
        try {
            //call modification_note(idNote,note,origine
            System.out.println(grade);
            System.out.println(idContract);
            if(isAlreadyInGrade(idContract, "Rapport")) {
                System.out.println("Rapport est déjà dans les notes");
                String call1 = "CALL modification_note (?,?,?)";
                CallableStatement procedure1 = connection.prepareCall(call1);
                procedure1.setInt(1,idContract);
                procedure1.setInt(2,grade.getReportMark());
                procedure1.setString(3,"Rapport");
                procedure1.executeUpdate();
                procedure1.close();
            } else {
                System.out.println("Rapport n'est pas dans les notes");
                String call2 = "CALL creation_note (?,?,?)";
                CallableStatement procedure2 = connection.prepareCall(call2);
                procedure2.setInt(1,idContract);
                procedure2.setInt(2,grade.getReportMark());
                procedure2.setString(3,"Rapport");
                procedure2.executeUpdate();
                procedure2.close();
            }
            if(isAlreadyInGrade(idContract, "These")) {
                System.out.println("These est déjà dans les notes");
                String call3 = "CALL modification_note (?,?,?)";
                CallableStatement procedure3 = connection.prepareCall(call3);
                procedure3.setInt(1,idContract);
                procedure3.setInt(2,grade.getThesisMark());
                procedure3.setString(3,"These");
                procedure3.executeUpdate();
                procedure3.close();
            } else {
                System.out.println("These n'est pas dans les notes");
                String call4 = "CALL creation_note (?,?,?)";
                CallableStatement procedure4 = connection.prepareCall(call4);
                procedure4.setInt(1,idContract);
                procedure4.setInt(2,grade.getThesisMark());
                procedure4.setString(3,"These");
                procedure4.executeUpdate();
                procedure4.close();
            }
            if(isAlreadyInGrade(idContract, "Travail")) {
                System.out.println("Travail est déjà dans les notes");
                String call5 = "CALL modification_note (?,?,?)";
                CallableStatement procedure5 = connection.prepareCall(call5);
                procedure5.setInt(1,idContract);
                procedure5.setInt(2,grade.getWorkMark());
                procedure5.setString(3,"Travail");
                procedure5.executeUpdate();
                procedure5.close();
            } else {
                System.out.println("Travail n'est pas encire dans les notes");
                String call6 = "CALL creation_note (?,?,?)";
                CallableStatement procedure6 = connection.prepareCall(call6);
                procedure6.setInt(1,idContract);
                procedure6.setInt(2,grade.getWorkMark());
                procedure6.setString(3,"Travail");
                procedure6.executeUpdate();
                procedure6.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@link DatabaseInteraction updatePassword}
     */
    @Override
    public void updatePassword(Person person,String newPassword) {
        try {
            //
            //UPDATE Utilisateur
            //SET  motDePasse_Ut = 'newPassword'
            //WHERE condition
            statement = connection.prepareStatement(String.format(
                    "UPDATE %s SET %s=? WHERE %s=?",
                    attribute.tableUser,
                    attribute.password,
                    attribute.idUt
            ));
            statement.setString(1, newPassword);
            statement.setInt(2, person.getUserID());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@link DatabaseInteraction endSession}
     * @param endConnection
     */
    @Override
    public void endSession(boolean endConnection) throws SQLException {
        statement.close();
        if (!connection.isClosed()){
            connection.close();
        }
    }
}