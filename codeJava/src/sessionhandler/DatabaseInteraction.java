package sessionhandler;

import model.admin.Contract;
import model.tool.Grade;
import model.tool.OrganismState;
import model.user.ExternalOrganism;
import model.user.Person;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Interface used for the database interaction
 */
interface DatabaseInteraction {
    /**
     * return a Person object when the credentials are valid and set the session and null object if not
     * @param username username as String
     * @param password passwords as String
     * @return Person
     * @throws SQLException when failure occurs
     * @label authenticate
     */
    Person authenticate(String username, String password) throws SQLException;

    /**
     * create from the database all the organizations and return an ArrayList
     * @return ArrayList of ExternalOrganism
     * @throws SQLException when failure occurs
     * @label listAllOrganizations
     */
    ArrayList<ExternalOrganism> listAllOrganizations() throws SQLException;

    /**
     * return an ExternalOrganism object
     * @param userID user id as int (Integer)
     * @return ExternalOrganism
     * @throws SQLException when failure occurs
     * @label organizationOf
     */
    ExternalOrganism organizationOf(Integer userID) throws SQLException;

    /**
     * return the organization id of a user
     * @param userId id of a user
     * @return organization id
     * @throws SQLException when failure occurs
     * @label getOrgaIdFromUserId
     */
    int getOrgaIdFromUserId(Integer userId) throws  SQLException;

    /**
     * return an ArrayList of Person objects related to an organization.
     * @param organismID organizations id
     * @return ArrayList of Person
     * @throws SQLException when failure occurs
     * @label listAllContactsFrom
     */
    ArrayList<Person> listAllContactsFrom(Integer organismID) throws SQLException;

    /**
     * return an ArrayList of Contract linked to a pupil
     * @param pupil Person object
     * @return Arraylist of Person
     * @throws SQLException when failure occurs
     * @label listAllContractsLinkedToPupil
     */
    ArrayList<Contract> listAllContractsLinkedToPupil(Person pupil) throws SQLException;

    /**
     * return an ArrayList of Contract linked to a contact
     * @param contact Person object
     * @return ArrayList of Person
     * @throws SQLException when failure occurs
     * @label listAllContractsLinkedToContact
     */
    ArrayList<Contract> listAllContractsLinkedToContact(Person contact) throws SQLException;

    /**
     * return an ArrayList of Person with a pupil function
     * @return ArrayList of Person
     * @throws SQLException when failure occurs
     * @label listAllPupils
     */
    ArrayList<Person> listAllPupils() throws SQLException;

    /**
     * return an ArrayList of Person with a contact function
     * @return ArrayList of Person
     * @throws SQLException when failure occurs
     * @label listAllContacts
     */
    ArrayList<Person> listAllContacts() throws SQLException;

    /**
     * return an ArrayList of Contract
     * @return ArrayList of Contract
     * @throws SQLException when failure occurs
     * @label listAllContracts
     */
    ArrayList<Contract> listAllContracts() throws SQLException;

    /**
     * return an ArrayList of Person
     * @return ArrayList of Person
     * @throws SQLException when failure occurs
     * @label listAllUsers
     */
    ArrayList<Person> listAllUsers() throws SQLException;

    /**
     * return an ArrayList of Grades linked to a pupil
     * @param person Person
     * @return  ArrayList of Grade
     * @throws SQLException when failure occurs
     * @label listAllGradesLinkedToPupil
     */
    ArrayList<Grade> listAllGradesLinkedToPupil(Person person) throws SQLException;

    /**
     * return an ArrayList of Grades linked to contact
     * @param person Person
     * @return ArrayList of Grade
     * @throws SQLException when failure occurs
     */
    ArrayList<Grade> listAllGradesLinkedToContact(Person person) throws SQLException;

    /**
     * ArrayList of all Grade objects
     * @return ArrayList of Grade
     * @throws SQLException when failure occurs
     * @label listAllGrades
     */
    ArrayList<Grade> listAllGrades() throws SQLException;

    /**
     * return the authorization level of a user
     * @param userID id of a user
     * @return authorization level as int (Integer)
     * @throws SQLException when failure occurs
     */
    int getAuthorizationByUserId(int userID) throws SQLException;

    /**
     * return a Person object after adding it to the database
     * @param lastname lastname of the person
     * @param firstname firstname of the person
     * @param organism ExternalOrganism of the person
     * @param phoneNumber phone number of the person
     * @param mailAddress mail address of the person
     * @param state the state of the contact
     * @return Person object
     * @throws SQLException when failure occurs
     * @label addContactToDatabase
     */
    Person addContactToDatabase(String lastname, String firstname, ExternalOrganism organism, String phoneNumber, String mailAddress, OrganismState state) throws SQLException;

    /**
     * add an extrnal organization to the database
     * @param organism ExternalOrganism
     * @label addOrgaToDatabase
     */
    void addOrgaToDatabase(ExternalOrganism organism) ;

    /**
     * update some data of a Person on the database
     * @param person Person object
     * @label updateContact
     */
    void updateContact(Person person);

    /**
     * update some data of an external organizations on the database
     * @param organism ExternalOrganism
     * @label updateOrganism
     */
    void updateOrganism(ExternalOrganism organism);

    /**
     * update the grade of a Contract on the database
     * @param grade Grade object
     * @param idContract id of the contract
     * @label updateGrade
     */
    void updateGrade(Grade grade, int idContract);

    //void updateUniversity(University university) throws SQLException;
    //void updateFirm(Firm firm) throws SQLException;
    //void updateContract(Contract contract) throws SQLException;

    /**
     * update the passwords of a user
     * @param person Person object
     * @param newPassword new
     * @label updatePassword
     */
    void updatePassword(Person person,String newPassword);

    /**
     * close the session and the connection to the database,only when the application is closing
     * @throws SQLException when failure occurs
     * @label endSession
     * @param endConnection Boolean
     */
    void endSession(boolean endConnection) throws SQLException;
}
