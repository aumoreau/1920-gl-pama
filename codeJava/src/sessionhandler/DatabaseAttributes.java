package sessionhandler;

/**
 * Class used to store the different table names and columns of the database. It's based on singleton pattern.
 */
public class DatabaseAttributes {
    /*Noms des tables */
    public final String tableOrgaExt = "OrganismeExt" ;
    public final String tableUser = "Utilisateur" ;
    public final String tableStudent = "Eleve" ;
    public final String tableStaff = "Personnel" ;
    public final String tableContract = "Contrat" ;
    public final String tableContact = "Contact" ;
    public final String tableMarks = "Note" ;
    /*Attributs de la table Utilisateur*/
    public final String idUt = "idUt_Ut";
    public final String identifiant = "identifiant_Ut";
    public final String password = "motDePasse_Ut";
    public final String lastname = "nom_Ut";
    public final String firstname = "prenom_Ut";
    public final String authorization = "niveauAutorisation_Ut";
    /*Attributs de la table Eleve*/
    public final String idEleve = "idEle_Ele";
    public final String idUt_Ele = "idUt_Ele";
    public final String graduationYear = "promotion_Ele";
    public final String sector = "filiere_Ele";
    /*Attributs de la table Personnel*/
    public final String idPersonnel = "idPers_Pers";
    public final String idUt_Personnel = "idUt_Pers";
    public final String fonction = "fonction_Pers";
    /*Attributs de la table Contact*/
    public final String idContact = "idContact_Cont";
    public final String idOrgaContact = "idOrga_Cont";
    public final String idUt_Contact = "idUt_Cont";
    public final String mail = "mail_Cont";
    public final String phone = "telephone_Cont";
    public final String etatContact = "etat_Cont";
    /*Attributs de la table Organisme_Ext*/
    public final String idOrga = "idOrga_Orga";
    public final String nameOrga = "nom_Orga";
    public final String adressOrga = "adresse_Orga";
    public final String cityOrga = "ville_Orga";
    public final String countryOrga = "pays_Orga";
    public final String siretOrga = "siret_Orga";
    public final String stateOrga = "etat_Orga";
    /*Attributs de la table Contrat*/
    public final String idContract = "idContrat_Ctr";
    public final String idContactContract = "idContact_Ctr";
    public final String idOrgaContract = "idOrga_Ctr";
    public final String idEleveContract = "idEle_Ctr";
    public final String typeContrat = "type_Ctr";
    public final String etatContract = "etat_Ctr";
    public final String beginning = "dateDebut_Ctr";
    public final String ending = "dateFin_Ctr";
    /*Attributs de la table Note*/
    public final String idMarks = "idNote_Note";
    public final String idContractNote = "idContrat_Note";
    public final String valuesMarks = "valeur_Note";
    public final String originMarks = "origine_Note";
    
    private DatabaseAttributes(){    }

    private static class ObjectHolder{
        private static final DatabaseAttributes attributes = new DatabaseAttributes() ;
    }

    /**
     * return the DatabaseAttributes object
     * @return DatabaseAttributes
     */
    public static DatabaseAttributes getDatabaseAttributes(){
        return ObjectHolder.attributes ;
    }
}
