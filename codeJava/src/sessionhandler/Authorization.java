package sessionhandler;

/**
 * Enum class to define the authorization level of the session
 */
public enum Authorization {
    visitor{ //En COURS
        @Override
        public String toString() {
            return "Visiteur";
        }
    },
    student{ //En COURS
        @Override
        public String toString() {
            return "Student";
        }
    },
    apprentice{ //En COURS
        @Override
        public String toString() {
            return "Apprentice";
        }
    },
    contact{ //En COURS
        @Override
        public String toString() {
            return "Contact";
        }
    },
    professor{ //En COURS
        @Override
        public String toString() {
            return "Professeur";
        }
    },
    departementOfEducation{ //En COURS
        @Override
        public String toString() {
            return "Departement Of Office member";
        }
    },
    externalRelationsResponsible{ //En COURS
        @Override
        public String toString() {
            return "External Relation Responsible";
        }
    },
    technicalOffice{ //En COURS
        @Override
        public String toString() {
            return "Technical Office member";
        }
    }
}
