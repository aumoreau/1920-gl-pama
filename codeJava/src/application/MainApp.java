package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.admin.Contract;
import model.user.ExternalOrganism;
import model.user.Person;
import sessionhandler.Authorization;
import sessionhandler.Session;
import view.*;

import java.io.IOException;
import java.sql.SQLException;

public class MainApp extends Application {

    private AnchorPane root;
    private Stage primaryStage;

    /**
     * This methode is used to setup the view for the authentification methode, and allows to put in motion the whole project.
     * @param stage
     * @out Void
     */
    @Override
    public void start(Stage stage) {

        if(!(Session.databaseConnection)) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.getDialogPane().getStylesheets().add("/view/graphic/DarkTheme.css");
            alert.setTitle("Erreur de connexion");
            alert.setHeaderText(null);
            alert.setContentText("Vous n'êtes pas connecté au réseau ENSSAT-PEDA");
            alert.showAndWait();
        }
        else {
            root = new AnchorPane();
            primaryStage = stage;
            primaryStage.setTitle("ERiP");
            primaryStage.getIcons().add(new Image("/view/graphic/Logo-erip.PNG"));
            primaryStage.setOnCloseRequest(we -> {
                Platform.exit();
                try {
                    Session.getSession().endSession(true);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            try {
                initAuthentification();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     *
     * */
    public void initMenu(boolean firstTime) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/MenuOverview.fxml"));
        root=loader.load();
        MenuController controller=loader.getController();
        controller.setMainApp(this);
        Scene scene=new Scene(root);
        if(Session.getSession().getAuthorization()!= Authorization.technicalOffice)
        {
            controller.getExportButton().setDisable(true);
            controller.getImportButton().setDisable(true);
            controller.getExportButton().setVisible(false);
            controller.getImportButton().setVisible(false);
        }
        primaryStage.setResizable(true);
        primaryStage.setScene(scene);
        if (firstTime) {
            primaryStage.centerOnScreen();
        }
        primaryStage.show();
    }

    public void initContactList(Person person, Stage stage, ExternalOrganism externalOrganism) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/ContactListOverview.fxml"));
        root = loader.load();
        ContactListController controller=loader.getController();
        controller.setMainApp(this);
        controller.setStage(stage);
        if(person==null && externalOrganism!=null) {
            controller.init(externalOrganism.getName());
        }
        else if (person!=null && externalOrganism==null)
        {
            controller.init(person.getLastName()+"");
            controller.getContactTableView().getSelectionModel().select(person);
            controller.showDetails(person);
        }
        else
        {
            controller.init("");
        }
        Scene scene=new Scene(root);
        stage.setScene(scene);
        if (stage!=primaryStage) {
            stage.setHeight(400);
            stage.setWidth(650);
            controller.getSearch().setDisable(true);
            controller.getCompany().setDisable(true);
        }
        stage.show();
    }

    public void initContractList(String initSearch, Stage stage) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/ContractListOverview.fxml"));
        root = loader.load();
        ContractListController controller=loader.getController();
        controller.setMainApp(this);
        controller.setStage(stage);
        controller.init(initSearch);
        Scene scene=new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    //avant de commencer, il faut savoir qu'en JavaFX, tout est noeud, et on pose des noeuds dans des noeuds dans des noeuds dans des noeuds, etc...
    //la fenêtre en sa totalité c'est un stage (souvent appelé primaryStage), et dedans on met une scène, une scène c'est un ensemble de noeuds

    //appelée lors de l'initialisation de l'affichage d'extérior organism (SANS DÉCONNER ???)
    public void initExteriorOrganismList(ExternalOrganism externalOrganism, Stage stage) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/ExternalOrganismListOverview.fxml"));    //les loaders ils servent à récupérer le code FXML et en faire un bel affichage java
        root = loader.load();                                                                                               //root c'est le nom du noeud principal, et c'est là qu'on pose notre bel affichage
        ExternalOrganismListController controller=loader.getController();                                                   //là on récupére les contrôleurs depuis le loader
        controller.setStage(stage);
        controller.setMainApp(this);                                                                                        //on donne le lien vers l'application principale à notre controller
        if (stage!=primaryStage) {
            controller.init(externalOrganism.getName());
        }
        else {
            controller.init("");
        }
        Scene scene=new Scene(root);                                                                                        //on crée une scène qu'on placera dans le stage quand on aura fini, et on donne le noeud principal à la scène en paramètre
        stage.setScene(scene);                                                                                              //on pose la scène dans notre stage
        if (stage!=primaryStage) {
            stage.setHeight(400);
            stage.setWidth(650);
            controller.getSearch().setDisable(true);
            controller.getOrganizationTableView().getSelectionModel().select(externalOrganism);
            controller.showDetails(externalOrganism);
        }
        stage.show();
    }

    public void initAuthentification() throws IOException
    {
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/AuthentificationOverview.fxml"));
        root = loader.load();
        AuthentificationController controller=loader.getController();
        controller.setMainApp(this);
        Scene scene=new Scene(root);
        scene.setOnKeyPressed(ke -> {
            if (ke.getCode() == KeyCode.ENTER) {
                controller.handlerConnectButton();
            }
        });

        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public void initAddContact(Stage stage, Person person, ExternalOrganism externalOrganism) throws IOException {
        AnchorPane root;
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/AddContactOverview.fxml"));
        root = loader.load();
        AddContactController controller=loader.getController();
        controller.setMainApp(this);
        controller.init(stage,person,externalOrganism);
        Scene scene=new Scene(root);
        stage.setScene(scene);
        scene.setOnKeyPressed(ke -> {
            if (ke.getCode() == KeyCode.ENTER) {
                controller.handlerConfirmButton();
            }
        });
        stage.centerOnScreen();
        stage.showAndWait();
    }

    public void initAddExternalOrganism(Stage stage,ExternalOrganism externalOrganism,ExternalOrganismListController parentController) throws IOException {
        AnchorPane node;
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/AddExternalOrganismOverview.fxml"));
        node = loader.load();
        AddExternalOrganismController controller=loader.getController();
        controller.setMainApp(this);
        controller.init(stage,externalOrganism,parentController);
        Scene scene=new Scene(node);
        stage.setScene(scene);
        scene.setOnKeyPressed(ke -> {
            if (ke.getCode() == KeyCode.ENTER) {
                controller.handlerConfirmButton();
            }
        });
        stage.centerOnScreen();
        stage.showAndWait();
    }

    public void initGrade(Stage stage, Contract contract) throws IOException {
        AnchorPane node;
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/GradeOverview.fxml"));
        node = loader.load();
        GradeController controller=loader.getController();
        controller.setMainApp(this);
        controller.init(stage,contract.getGrade(),contract);
        Scene scene=new Scene(node);
        stage.setScene(scene);
        scene.setOnKeyPressed(ke -> {
            if (ke.getCode() == KeyCode.ENTER) {
                controller.handlerConfirmButton();
            }
        });
        stage.centerOnScreen();
        stage.showAndWait();
    }

    public void initChangePassword(Stage stage,String password) throws IOException {
        AnchorPane node;
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/ChangePasswordOverview.fxml"));
        node = loader.load();
        ChangePasswordController controller=loader.getController();
        controller.setMainApp(this);
        controller.init(stage,password);
        Scene scene=new Scene(node);
        stage.setScene(scene);
        stage.setResizable(false);
        scene.setOnKeyPressed(ke -> {
            if (ke.getCode() == KeyCode.ENTER) {
                controller.handlerChangeButton();
            }
        });
        stage.centerOnScreen();
        stage.showAndWait();
    }

    public void initEasterEgg()
    {
        AnchorPane node=null;
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/EasterEggOverview.fxml"));
        try {
            node = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert node != null;
        Scene scene=new Scene(node);
        stage.setScene(scene);
        stage.showAndWait();
    }

    public void initKeepYourPassword()
    {
        AnchorPane node=null;
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/view/KeepOverview.fxml"));
        try {
            node = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert node != null;
        Scene scene=new Scene(node);
        stage.setScene(scene);
        stage.showAndWait();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {launch(args);}
}
